# Représentations graphiques

## Noms de code

Les représentations graphiques peuvent avoir de longs titres ; exemple : *Carte des erreurs des sites de communes de
+10k habitants, agrégées par département*. Il est laborieux de manipuler de telles chaines de caractères. Pour
faciliter le nommage, nous utiliserons des noms de code.

Chaque représentation graphique, en plus de son titre (affiché sur le graphique), aura un nom de code.

## Nomenclature des noms de code

Nous utiliserons comme nomenclature (dictionnaire) la [liste des îles de France](https://fr.wikipedia.org/wiki/Liste_des_%C3%AEles_de_France)

- [x] Bananec
- [ ] Penfret
- [ ] Drenec
- [ ] Guéotec
- [ ] Chausey
- [ ] Batz
- [ ] Arz
- [ ] Govihan
- [ ] Ilur
- [ ] Brescou
- [x] If
- [x] Planier
- [ ] Petit Gaou
- [ ] Petit Langoustier
- [ ] Grand Ribaud
- [ ] Porquerolles
- [ ] Lavezzi
- [ ] Sanguinaire
- [ ] Giraglia
- [ ] Fazziolu
- [ ] Tromelin
- [ ] Glorieuse
- [ ] Juan de Nova
- [ ] Bassas da India
- [ ] Europa
- [ ] Saint Paul
- [ ] Nouvelle Amsterdam
- [ ] Crozet
- [ ] Kerguelen
- [ ] Wallis
- [ ] Futuna
- [ ] Réunion
- [ ] Bora
- [ ] Raiatea
- [ ] Clipperton
- [ ] Marquise
- [ ] Tuamotu
- [ ] Fakarava
- [ ] Maupiti
- [ ]
- [ ]
