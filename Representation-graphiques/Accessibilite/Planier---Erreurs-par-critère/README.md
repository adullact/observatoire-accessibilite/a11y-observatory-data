# Accessibilité : nombre d'erreurs par critère RGAA

Nom de code : Planier

## Requête SQL

```sql
SELECT cr.Label as Critere, cr.Id_Criterion, min(cr.Rank) as Rang, count(cr.Id_Criterion) as Erreurs
FROM CRITERION as cr
         INNER JOIN TEST as t ON t.Id_criterion = cr.Id_Criterion
         INNER JOIN PROCESS_RESULT as pr ON pr.Id_Test = t.Id_Test
         INNER JOIN WEB_RESOURCE as wr ON wr.Id_Web_Resource = pr.Id_Web_Resource
         INNER JOIN AUDIT as a ON a.Id_Audit = wr.Id_Audit
WHERE pr.Definite_Value = 'FAILED'
  AND a.STATUS = 'COMPLETED'
GROUP BY Id_Criterion
ORDER BY Rang
;
```

## Dataset

1. Saisir la requête dans *SQL Lab*, l'exécuter, puis la sauvegarder sous le nom `Erreurs par critères`
2. Cliquer sur le bouton *Create Chart* (sous la zone de saisie SQL)
3. Sauver en tant que nouveau dataset nommé `Erreurs par critères` (un nouvel onglet s'ouvrira juste ensuite)
4. En haut de l'écran, nommer le graphe : `Erreurs par critères RGAA`
5. Configuration du graphe, onglet Data :
    * En haut, nommer le graphe : `Erreurs par critères RGAA`
    * *Visualisation type* :
        * cliquer sur *View all charts*,
        * saisir `bar chart`,
        * cliquer sur *Bar Chart* et valider
    * *Metric* : onglet *Simple*, pour *Column* choisir `Erreurs`, pour *Aggregate* choisir *Sum*, et valider
    * *Dimensions* : onglet *Simple*, pour *Column* choisir `Critere`
    * *Sort by* : onglet *Simple*, pour *Column* choisir *Rang*, pour *Aggregate* choisir *Min*, et valider
    * Décocher *Sort descending*
    * Cliquer sur *Update chart*
6. Configuration du graphe, onglet Customize :
    * Color scheme : choisir un schéma avec une première couleur vive (rose / orange)
    * Cocher *Bar values*
    * *Y axis format* : choisir `.3s`
    * *Y axis label* : saisir `Erreurs`
    * *X axis label* : saisir `Critère RGAA`
    * *X tick layout* : choisir `45°`
7. Sauvegarder le graphe sous le nom *Erreurs par critères RGAA*, et l'ajouter au *tableau de bord* `Accessibilité` (à
   créer si pas déjà fait)

## Tableau de bord

1. Créer un tableau de bord
2. Ajouter le graphe dedans.
