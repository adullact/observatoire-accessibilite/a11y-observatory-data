# Carte des erreurs des sites de communes de +10k habitants, agrégées par département

Nom de code : Bananec

## Sources de données utilisées

- API Établissements Publics (source `Api.Gouv.Fr`), pour lister le nom des communes et leur URL
- Population légale 2019 (source INSEE), afin d'avoir la population de chaque commune
- Noms de domaines secteur public (source DINUM), pour lister les URLS des communes
- Audit Asqatasun de 18k+ URLs de communes, afin d'avoir pour chaque URL le nombre d'erreurs d'accessibilité

## Asqatasun Requête SQL

Pour information uniquement. Cette requête est intégrée dans le script Python

```sql
SELECT a.Id_Audit,
       a.Dt_Creation,
       wr.Url,
       wrs.Nb_Failed_Occurrences as erreurs
FROM AUDIT as a
         INNER JOIN AUDIT_TAG as at on a.Id_Audit = at.Id_Audit
         INNER JOIN TAG as t on t.Id_Tag = at.Id_Tag
         INNER JOIN WEB_RESOURCE_STATISTICS wrs on a.Id_Audit = wrs.Id_Audit
         INNER JOIN WEB_RESOURCE wr on a.Id_Audit = wr.Id_Audit
WHERE a.Status = 'COMPLETED'
  AND t.Value = '2023-03-09-22h17_communes18k_firefox15_sleep0'
;
```

## Génération des données pour le graphe

Le fichier `communes_10k_hab_url_population_erreurs.csv` est produit par le
script `communes_10k_hab_url_population_erreurs.py`.

Pour l'exécuter avec Python 3.10 :

```shell
pip install -r requirements.txt
/usr/bin/python3.10 communes_10k_hab_url_population_erreurs.py
```

## Création du graphe dans Superset

1. Importer le fichier `communes_10k_hab_url_population_erreurs.csv` dans une base
2. Cliquer sur le dataset généré pour créer un graphe
3. Choisir un graphe de type *Country map"
    - Country: `France`
    - ISO 3166-2 Codes: `iso3166-2-dpt`
    - Metric: `MIN(tranche_mediane_erreurs)` (la fonction d'agrégation importe peu)
4. Ajouter le titre : `Erreurs d'accessibilité des communes de +10000 habitants, médiane agrégée par département`
