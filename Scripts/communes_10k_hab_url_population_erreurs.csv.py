import logging
import sys
from pathlib import Path
from urllib.parse import urlparse

import pandas as pd
from decouple import config
from sqlalchemy import URL
from sqlalchemy import create_engine

repo_root = Path(__file__).parent.parent

# communes dataset, origin = api.gouv.fr
communes = repo_root / "source-data/api.gouv.fr/etablissements-publics/communes-2023-04-18.csv"
# population dataset, origin = INSEE
population = repo_root / "source-data/INSEE/Population-2019/Donnees/donnees_communes.csv"
# Output dataset
communes_population = repo_root / "source-data/Jeu_donnees_Commune/Communes-2023-codeInsee-url-population-2019.csv"
communes_url_population = repo_root / "Representation-graphiques/Collectivites/Communes-10k-hab-Carte-Erreurs" \
                                      "-agregees-par-departement/communes_10k_hab_url_population_erreurs.csv"

# DB used for "communes"
communes_db = "asqatasun_20230310_communes_18k"
# Tag used for the audit campaign on "communes"
communes_audit_tag = "2023-03-09-22h17_communes18k_firefox15_sleep0"

# Minimum inhabitant for cities
min_population = 10000


def convert_url_to_domain(url):
    """Return the domain of a URL"""
    return urlparse(url).netloc


def dataset_csv_export(d, output):
    """Export dataset to CSV"""
    print(d.head())
    d.to_csv(output)
    logging.getLogger().info('Dataset exported to ' + str(output))


def prepare_external_data(commune_file, population_file):
    """Grab and merge data on "communes" and population"""

    # Load "communes" file (origin: api.gouv.fr)
    df1 = pd.read_csv(commune_file)
    # Load population file (origin: INSEE)
    df2 = pd.read_csv(population_file, delimiter=";")

    # Rename column "COM" to "nom" to be able to merge with "communes"
    df2 = df2.rename(columns={'COM': 'nom'})

    # merge two datasets based on common column, i.e. "nom"
    dataset = pd.merge(df1, df2, on='nom', how='inner')

    # Data analyst good practice: always drop duplicates, as they almost always exist
    dataset.drop_duplicates(subset="url", keep='first', inplace=True)

    # Build department code compliant with ISO-3166-2
    dataset["iso3166-2-dpt"] = 'FR-' + dataset["CODDEP"]

    # Build domain (will be used to merge with Asqatasun data)
    dataset["domain"] = dataset["url"].apply(lambda x: convert_url_to_domain(x))

    # Remove useless columns
    dataset = dataset.drop(columns=['CODREG', 'REG', 'CODDEP', 'CODARR', 'CODCAN', 'CODCOM', 'PMUN', 'PCAP'])

    # Set a human-understandable name to columns
    dataset = dataset.rename(columns={'PTOT': 'Population'})

    return dataset


def prepare_asqatasun_data(db, tag):
    """Grab and prepare data from Asqatasun audit campaign"""

    query = f"""
SELECT a.Id_Audit,
       a.Dt_Creation,
       wr.Url as url_asqa,
       wrs.Nb_Failed_Occurrences as erreurs
FROM AUDIT as a
         INNER JOIN AUDIT_TAG as at on a.Id_Audit = at.Id_Audit
         INNER JOIN TAG as t on t.Id_Tag = at.Id_Tag
         INNER JOIN WEB_RESOURCE_STATISTICS wrs on a.Id_Audit = wrs.Id_Audit
         INNER JOIN WEB_RESOURCE wr on a.Id_Audit = wr.Id_Audit
WHERE a.Status = 'COMPLETED'
  AND t.Value = '{tag}'
;
    """

    url_object = URL.create(
        "mysql+pymysql",
        username=config('ASQATASUN_DB_USER'),
        password=config('ASQATASUN_DB_PASSWORD'),
        host=config('ASQATASUN_DB_HOST'),
        port=config('ASQATASUN_DB_PORT'),
        database=db,
    )
    engine = create_engine(url_object)

    asqa_result = pd.read_sql(query, engine)
    asqa_result["domain"] = asqa_result["url_asqa"].apply(lambda x: convert_url_to_domain(x))
    return asqa_result


def build_final_data(asqa_data, external_data):
    result = pd.merge(asqa_data, external_data, how='inner', on='domain')

    # Remove useless columns
    result = result.drop(columns=['url'])

    # Data analyst good practice: always drop duplicates, as they almost always exist
    result.drop_duplicates(subset="domain", keep='first', inplace=True)

    # Keep only cities bigger than 10k inhabitants
    result2 = result.loc[result['Population'] >= min_population]

    # To compute median, data must be sorted beforehand
    result3 = result2.sort_values(by='erreurs', ascending=True)
    # Compute median of errors for each department
    result4 = result3.groupby('iso3166-2-dpt')[['erreurs']].median().reset_index()
    result4 = result4.rename(columns={'erreurs': 'mediane_erreurs'})

    # Attribute to each entry a category depending on its number of errors
    tranche_mediane_erreurs = []
    for value in result4['mediane_erreurs']:
        if value < 10:
            tranche_mediane_erreurs.append(0)
        elif 10 <= value < 30:
            tranche_mediane_erreurs.append(10)
        elif 30 <= value <= 60:
            tranche_mediane_erreurs.append(30)
        else:
            tranche_mediane_erreurs.append(60)
    result4["tranche_mediane_erreurs"] = tranche_mediane_erreurs

    dataset_csv_export(result4, communes_url_population)


def main():
    logging.basicConfig(stream=sys.stdout, level=logging.INFO, format='%(asctime)s %(message)s')

    dataset_external = prepare_external_data(communes, population)
    dataset_asqa = prepare_asqatasun_data(communes_db, communes_audit_tag)
    build_final_data(dataset_asqa, dataset_external)


if __name__ == "__main__":
    main()
