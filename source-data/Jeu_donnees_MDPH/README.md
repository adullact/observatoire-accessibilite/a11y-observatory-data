# Construction de la base de données des Maisons Départementales des Personnes Handicapées (MDPH)

Pour créer la base de données des MDPH, nous allons utiliser 4 fichiers :

- **Fichier 1** : [Maisons des handicapées](http://etablissements-publics.api.gouv.fr/v3/organismes/maison_handicapees)
  - [Convertir le fichier json](https://products.aspose.app/cells/conversion)
  - Dans LibreOffice, séparer les codes communes et les communes

```libreoffice
# Supprimer les premier caractere
=DROITE(A1;NBCAR(A1)-6)

# Garder les 1er caractere
=GAUCHE(T1;5)
```

- **Fichier 2** : [Commune et département](https://www.insee.fr/fr/information/2028028)
  - Télécharger le 1er dossier : Table d’appartenance géographique des communes au 1er janvier 2022 (zip)
  - Supprimer les colonnes qui nous intéresse pas et garder : Code_commune, commune, code_département et code_région
- **Fichier 3** : [MDPH avec les départements sans doubons](MDPH_departement_.csv)
- **Fichier
  4
  ** : [AAH pour la caf](http://data.caf.fr/dataset/personnes-percevant-l-allocation-aux-adultes-handicapes-aah-par-caf)
  - Fichier CSV à télécharger : AAH - par caf
  - Avant d'intégrer ce fichier dans python, nous allons supprimer les lignes qui ne nous intéressent pas et nous
    choisirons comme date la plus récente et supprimer les autres.

```Python
# Library utiliser
import pandas as pd
import numpy as np

# Lire fichier 1
df = pd.read_csv("maison_handicapees_.csv", delimiter=",", low_memory=False)

# Supprimer les cases vides
df = df[df['URL'].notna()]

# Supprimer des colonnes
df = df.drop(columns=['Communes'])

# Lire le fichier 2
df2 = pd.read_csv("commune_departement.csv", delimiter=",", low_memory=False)

# Supprimer et renommer les colonnes
df2 = df2.drop(columns=['Code_Commune'])
df2 = df2.rename(columns={'Communes': 'Commune'})

# Fusionner les 2 fichiers
df3 = pd.merge(df1, df2)

# Regarder s'il y a des doublons
dups = df3.pivot_table(index=['URL'], aggfunc='size')

# S'il y a des doublons convertir en csv et supprimer à la main
dups.to_csv('compter_doublons.csv')

# Intégrer les fichiers sans doublons : Fichier 3
df4 = pd.read_csv("MDPH_departement_.csv", encoding='latin_1', delimiter=",", low_memory=False)

# Lire le fichier 4
df5 = pd.read_csv("aahcaf.csv", encoding='latin_1', delimiter=";", low_memory=False)

# Fusionner avec les fichiers
df6 = pd.merge(df4, df5)

# Lire le fichier 3
df3 = pd.read_csv("cg_dep.csv", delimiter=",", low_memory=False)

# Fusionner
df4 = pd.merge(df3, df2)

# Réorganiser les colonnes
df6 = df6[
  ['Code_INSEE', 'Properties_ID', 'Code_Region', 'Departement', 'Code_Dpt', 'Code_Commune', 'Commune', 'Code_Postal',
   'MDPH', 'Longitude', 'Latitude', 'Effectif']]

# Convertir en csv
df6.to_csv('MDPH_departement_effectif.csv')
```

## Résultat de la base de donnée des MDPH

[Base de donnée des MDPH](MDPH_departement_effectif.csv)
