# Construction de la base de donnée des Hôpitaux publics

Pour créer la base de données des hôpitaux publics, nous allons utiliser 2 fichiers :

**Fichier 1** : [Établissement public de santé](https://www.wikidata.org/wiki/Q3591565)

```sql
SELECT DISTINCT ?class ?classLabel ?item ?itemLabel ?url ?Localisation
WHERE
  {
  ? class wdt:P279* wd:Q3591565.
  ?item wdt:P31 ? class.
  ?item wdt:P856 ?url.
  ?item wdt:P131 ?Localisation.
  SERVICE wikibase: label { bd:serviceParam wikibase: language "fr".}
  }
LIMIT 100000
```

**Fichier 2** : [Population](https://www.insee.fr/fr/statistiques/6011070?sommaire=6011075)

- Dossier : France métropolitaine et DOM
- Fichier : `donnees_communes.csv`

```python
# Library utiliser
import pandas as pd
import numpy as np

# Lire fichier 1
df = pd.read_csv("Wiki_URL.csv", delimiter=",", low_memory=False)

# Lire le fichier 2
df1 = pd.read_csv("population2019.csv", delimiter=",", low_memory=False)

# Fusionner les 2 fichiers
df2 = pd.merge(df, df1)

# Regarder s'il y a des doublons
dups = df2.pivot_table(index=['URL'], aggfunc='size')

# S'il y a des doublons convertir en csv et supprimer à la main
dups.to_csv('doublons.csv')

# Convertir en csv
df2.to_csv('hopitaux_departement_population.csv')
```

Le fichier doublons.csv, nous permet de lister les doublons et de les supprimer sur le fichier
`hopitaux_departement_population.csv`.
Ces doublons sont à cause de quelque nom de commune qui sont identiques pour différents département ou région.
Exemple la commune 'La Rochelle' existe dans la région Nouvelle-Aquitaine et Bourgogne-Franche-Comté.

## Résultat de la base de donnée des hôpitaux

[Base de donnée des hôpitaux](hopitaux_departement_population.csv)
