# Source de données de la population

Sont regroupées ici toutes les données concernant la population.

## Recensement de la population

### 1. Populations légales 2019 (INSEE)

- Source : <https://www.insee.fr/fr/statistiques/6011070?sommaire=6011075>
- Dossier : France métropolitaine et DOM
- Fichier : donnees_communes.csv
  - La population totale par commune

![](Screenshot_population_commune.png)

### 2. Données decoupage administratif COG

- Source : <https://github.com/etalab/decoupage-administratif>
- Dépôt : <https://github.com/etalab/decoupage-administratif/tree/master/sources>
- Dossier : <https://github.com/etalab/decoupage-administratif/blob/master/sources/population2019.xls.gz>
- Fichier : population2019.xls
  - Onglet commune : Liste de la population par commune
  - Onglet département : Liste de la population par départements

![](Screenshot_population_2019.png)

## Âge de la population

### 1. Estimations de population par sexe et âge au 1ᵉʳ janvier 2022Comparaisons régionales et départementales (INSEE)

- Source : <https://www.insee.fr/fr/statistiques/2012692>
- Fichier : `TCRD_021.XLSX`
  - Onglet 1 : Liste des départements en fonction de la population et l'âge

![](Screenshot_population_age.png)

### 2. Démographie - Recensement de la Population par IRIS (data.gouv)

- Source : <https://www.data.gouv.fr/fr/datasets/demographie-recensement-de-la-population-par-iris/>
- Dossier : Export au format CSV
- Fichier : `demographie-recensement-de-la-population-population-par-iris.csv`

![](Screenshot_démographie_population.png)

### 3. Population selon le sexe et l'âge quinquennal de 1968 à 2018 (1990 à 2018 pour les DOM)

- Source : <https://www.insee.fr/fr/statistiques/1893204>
- Fichier : `pop-sexe-age-quinquennal6818.xls`
  -Onglet COM_2018 : liste de l'age de la population par commune

![](Screenshot_pop_age.png)

## Revenue de la population

### 1. Data INSEE sur les communes (data.gouv)

- Source: <https://www.data.gouv.fr/fr/datasets/data-insee-sur-les-communes/>
- Dossier : DATA INSEE commune
- Fichier: `MDB-INSEE-V2.xls`
  - Liste des revenus moyens par communes avec région, département, population..

### 2. Revenus des Français à la commune (data.gouv)

- Source : <https://www.data.gouv.fr/fr/datasets/revenus-des-francais-a-la-commune/>
- Fichier : `Niveau_de_vie_2013_a_la_commune-Global_Map_Solution.xlsx`
  - Niveau de vie des français par commune et département

![](Screenshot_revenus_par_commune.png)
