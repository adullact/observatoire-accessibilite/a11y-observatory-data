# Construction de la base de donnée des Allocations aux Adultes Handicapés (AAH) pour les communes

Pour créer la base de données des AAH des communes, nous allons utiliser deux 2 fichiers :

- **Fichier 1** :
  [AAH_commune](http://data.caf.fr/dataset/beneficiaires-de-l-allocation-aux-adultes-handicapes-au-cours-du-mois-de-decembre-par-epci)
  - Fichier CSV à télécharger: AAH
  - Avant d'intégrer ce fichier dans Python, nous allons supprimer les lignes qui ne nous intéressent pas et nous
    choisirons comme date la plus récente et supprimer les autres.
- **Fichier 2** : [audit_commune](audit_communes.csv)

```Python
# Library utiliser
import pandas as pd
import numpy as np

# Lire fichier 1
df = pd.read_csv("AAH_Commune.csv", delimiter=";", low_memory=False)

# Supprimer des colonnes
df = df.drop(columns=['Communes', 'Code_Dpt'])

# Lire le fichier 2
df1 = pd.read_csv("audit_communes.csv", delimiter=",", low_memory=False)

# Supprimer les colonnes
df1 = df1.drop(columns=['Population municipale', 'Population comptée à part', 'Population totale',
                        '20_39_ans', '40_59_ans', 'plus_60_ans'])

# Fusionner les 2 fichiers
df2 = pd.merge(df1, df)

# Supprimer les colonnes vide
df2 = df2.dropna()

# Réorganiser les colonnes
df2 = df2[['Id_Audit', 'Id_Web_Resource', 'Code_INSEE', 'Code_Region', 'Region',
           'Code_Dpt', 'Departement', 'Code_Commune', 'Commune', 'Code_Postale', 'Latitude',
           'Longitude', 'URL', 'Nb_Failed', 'Mark', 'Mark_Letter', 'Nombre_Occurence_Erreurs', 'NB_Allocataires',
           'ALL_AAH', 'Date']]

# Convertir en csv
df2.to_csv('AAH_audit_commune.csv')
```

## Résultat de la base de donnée des AAH pour les collectivités

[Base de donnée des AAH des collectivités](AAH_audit_commune.csv)
