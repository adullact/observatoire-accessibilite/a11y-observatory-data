# AAH Allocation Adulte Handicapé

## AAH sur data.caf.fr

<http://data.caf.fr/dataset?groups=aah>

## Allocataires de l’AAH (INSEE)

- Source : <https://www.insee.fr/fr/statistiques/2382595?sommaire=2382915>
- Dossier zip : `ISD-HA-juin2022.zip`
- Fichier : `HA05-ISD_Taux_allocataires_AAH.xlsx`

![](Screenshot_AAH.png)
