# Construction de la base de donnée des Allocations aux Adultes Handicapés (AAH) pour les régions

Pour créer la base de données des AAH des régions, nous allons utiliser 3 fichiers :

- **Fichier 1** :
  [AAH par région](https://drees2-sgsocialgouv.opendatasoft.com/explore/dataset/627_personnes-en-situation-de-handicap/information/)
  - dossier à télécharger : `ISD_Personnes en situation de handicap-Niveau régional.zip`
  - Fichier xlsx : `HA05-REG_Taux_allocataires_AAH.xlsx`
  - Dans LibreOffice, supprimer les lignes inutiles
- **Fichier 2** : [Conseils régionaux](https://etablissements-publics.api.gouv.fr/v3/organismes/cr)
  - [Convertir le fichier json](https://products.aspose.app/cells/conversion)
  - Liste des régions, code_insee, longitude, latitude et url

```Python
# Library utiliser
import pandas as pd
import numpy as np

# Lire fichier 1
df = pd.read_csv("HA05-REG_Taux_allocataires_AAH_Region.csv", delimiter=",", low_memory=False)

# Lire le fichier 2
df1 = pd.read_csv("region_.csv", delimiter=",", low_memory=False)

# Fusionner
df2 = pd.merge(df1, df)

# Réorganiser les colonnes
df2 = df2[['Code_INSEE', 'Properties_ID', 'Code_Region', 'Region', 'Code_Postal', 'Adresse',
           'Longitude', 'Latitude', 'URL',
           'Part des allocataires AAH au 31/12/2020\n dans la population des 20 à 64 ans (%)']]

# Convertir en csv
df2.to_csv("AAH_reg.csv")
```

## Résultat de la base de donnée des AAH pour les régions

[Base de donnée des AAH pour les régions](AAH_reg_.csv)
