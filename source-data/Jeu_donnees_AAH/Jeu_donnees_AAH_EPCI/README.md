# Construction de la base de donnée des Allocations aux adultes handicapés (AAH) pour les EPCI

Pour créer la base de données des AAH des EPCI, nous allons utiliser 2 fichiers :

- **Fichier 1** :
  [AAH par EPCI](http://data.caf.fr/dataset/beneficiaires-de-l-allocation-aux-adultes-handicapes-au-cours-du-mois-de-decembre-par-epci)
  - Fichier csv à télécharger : AAH
  - Dans LibreOffice, supprimer les lignes inutiles et nous choisirons
    comme date la plus récente et supprimer les autres.
- **Fichier 2** : [EPCI](EPCI_dep_.csv)

```Python
# Library utiliser
import pandas as pd
import numpy as np

# Lire fichier 1
df = pd.read_csv("AAH_EPCI.csv", delimiter=";", low_memory=False)

# Supprimer les colonnes
df = df.drop(columns=['Lib_EPCI', 'DEPSEPCI', 'DTREF'])

# Lire le fichier 2
df1 = pd.read_csv("EPCI_dep_.csv", delimiter=",", low_memory=False)

# Supprimer les colonnes
df1 = df1.drop(columns=['nj_epci2022', 'fisc_epci2022', 'nb_com_2022', 'ptot_epci_2022', 'pmun_epci_2022'])

# Fusionner
df2 = pd.merge(df1, df)

# Réorganiser les colonnes
df2 = df2[['Code_INSEE', 'Id_EPCI', 'EPCI', 'Lib_EPCI', 'Code_Region', 'Région', 'Code_Dpt',
           'Département', 'Commune', 'Code_Commune', 'Latitude', 'Longitude', 'URL', 'NB_Allocataires', 'ALL_AAH']]

# Convertir en csv
df2.to_csv("AAH_EPCI_.csv")
```

## Résultat de la base de donnée des AAH pour les EPCI

[Base de donnée des AAH pour les EPCI](AAH_EPCI_.csv)
