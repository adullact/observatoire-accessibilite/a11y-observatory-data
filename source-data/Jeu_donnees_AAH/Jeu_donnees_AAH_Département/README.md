# Construction de la base de donnée des Allocations aux Adultes Handicapés (AAH) pour les départements

Pour créer la base de données des AAH des départements, nous allons utiliser 3 fichiers :

- **Fichier 1** :
  [AAH par département](https://drees2-sgsocialgouv.opendatasoft.com/explore/dataset/627_personnes-en-situation-de-handicap/information/)
  - Fichier xlsx à télécharger : `HA05-ISD_Taux_allocataires_AAH_departement.xlsx`
  - Dans LibreOffice, supprimer les lignes inutiles
- **Fichier 2** :
  [AAH pour la caf](http://data.caf.fr/dataset/personnes-percevant-l-allocation-aux-adultes-handicapes-aah-par-caf)
  - Fichier CSV à télécharger : AAH - par caf
  - Avant d'intégrer ce fichier dans Python, nous allons supprimer les lignes qui ne nous intéressent pas et nous
    choisirons comme date la plus récente et supprimer les autres.
- **Fichier 3** : [Conseils départementaux](https://etablissements-publics.api.gouv.fr/v3/organismes/cg)
  - [Convertir le fichier json](https://products.aspose.app/cells/conversion)
  - Liste des départements, code_insee, longitude, latitude et url

```Python
# Library utiliser
import pandas as pd
import numpy as np

# Lire fichier 1
df = pd.read_csv("HA05-ISD_Taux_allocataires_AAH_departement.csv", delimiter=",", low_memory=False)

# Lire le fichier 2
df1 = pd.read_csv("aahcaf.csv", encoding='latin', delimiter=";", low_memory=False)

# Supprimer les colonnes
df1 = df1.drop(columns=['Code_Dpt'])

# Fusionner les 2 fichier
df2 = pd.merge(df1, df)

# Lire le fichier 3
df3 = pd.read_csv("cg_dep.csv", delimiter=",", low_memory=False)

# Fusionner
df4 = pd.merge(df3, df2)

# Réorganiser les colonnes
df4 = df4[['Code_INSEE', 'Code_Region', 'Code_Dpt', 'Departement', 'Commune', 'Code_Postal',
           'Adresse', 'Longitude', 'Latitude', 'URL', 'Effectif',
           'Part des allocataires AAH\n dans la population des 20 à 64 ans (%)']]

# Convertir en csv
df4.to_csv('AAH_dep.csv')
```

## Résultat de la base de donnée des AAH pour les départements

[Base de donnée des AAH pour les départements](AAH_dep.csv)
