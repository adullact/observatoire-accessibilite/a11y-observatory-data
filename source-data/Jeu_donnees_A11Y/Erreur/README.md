# Création de la base de données des erreurs Asqatasun pour l’accessibilité

Pour pouvoir afficher les erreurs Asqatasun, nous allons écrire une requête sur Superset

## Requêtes SQL

```sql
SELECT
  t.Label ,
  COUNT(prm.Message_Code) as nb

FROM PROCESS_REMARK AS prm
INNER JOIN PROCESS_RESULT AS prs ON prs.Id_Process_Result = prm.Id_Process_Result
INNER JOIN AUDIT AS a ON a.Id_Audit = prs.Id_Audit_Net_Result
INNER JOIN AUDIT_TAG AS at ON at.Id_Audit = a.Id_Audit
INNER JOIN TAG AS ta ON ta.Id_Tag = at.Id_Tag
INNER JOIN TEST AS t ON t.Id_Test = prs.Id_Test

WHERE
  a.status = "COMPLETED"
  AND ta.Value = '2023-01-25_epci_18h42_firefox5_sleeps'
  AND prm.Issue ='FAILED'
  AND prs.Definite_Value = 'FAILED'

GROUP BY  t.Id_Test
;
```

Même requêtes SQL pour les communes et départements, il faut juste changer la valeur du tag.

- Pour les communes : ta.value = '2023-03-09-22h17_communes18k_firefox15_sleep0'
- Pour les départements : ta.value = '2023-03-13-14h17_conseils_departementaux_firefox5_sleep0'

Une fois que nous avons tous les requêtes nous allons regrouper ses valeurs sur python.

```python
# Librairies utilisés
import pandas as pd
import numpy as np
from datetime import datetime

# Lire le fichier des communes
df=pd.read_csv("Nbre_occurence_erreur_commune.csv", delimiter=",", low_memory=False)

# Lire le fichier des EPCI
df1=pd.read_csv("Nbre_occurence_erreur_EPCI.csv", delimiter=",", low_memory=False)

# Lire le fichier des départements
df2=pd.read_csv("Nbre_occurence_erreur_departement.csv", delimiter=",", low_memory=False)

# Fusionner le fichier des communes et EPCI
df3=pd.concat([df, df1])

# Fusionner les fichier avec le fichier des départements
df4=pd.concat([df3, df2])

# Remplacer les valeurs des labels des tests par critère
df4['Label']=df4['Label'].replace({'10.1.1': '10.1', '10.1.2': '10.1', '3.2.1': '3.2', '3.2.2': '3.2',
'3.2.3': '3.2', '3.2.4': '3.2', '6.1.1': '6.1', '6.1.2': '6.1', '6.1.3': '6.1', '6.1.4': '6.1',
'9.1.1': '9.1', '9.1.2': '9.1', '13.1.2': '13.1', '2.1.1': '2.1', '2.2.1':'2.2', '8.1.2':'8.1',
'8.10.2':'8.10', '8.3.1': '8.3', '8.4.1': '8.4', '8.5.1': '8.5', '8.6.1': '8.6', '8.8.1': '8.8',
'8.9.1': '8.9', '9.2.1': '9.2' })

# Regrouper les critères
df4=df4.groupby(['Label'])[['nb']].sum().reset_index()

# Convertir en csv
df4.to_csv('Nombre_erreur_label.csv')
```

## Résultat

Statistique A11Y : [Nombre d'erreurs des tests](Nombre_erreur_label.csv)
