# Déclaration accessibilité

## Dashlord --> automatic detection of the a11y declaration for French websites

- <https://dashlord.incubateur.net/>
- <https://github.com/SocialGouv/dashlord>
- <https://github.com/SocialGouv/dashlord-actions>

Example : <https://dashlord.incubateur.net/url/aidantsconnect-beta-gouv-fr/best-practices/#declaration-a11y>

Detection source code:
<https://github.com/SocialGouv/dashlord-actions/blob/main/declaration-a11y/index.js>

## Automatic detection of the a11y declaration for French websites (script examples)

Vérifie la présence des mentions obligatoires RGAA 4 de la page d’accueil pour une liste d'URL :

<https://github.com/ele-gall-ac-mineducation/projets/blob/master/mention_RGAA_URLs.py>

Recherche de déclaration d’accessibilité pour une URL :

<https://github.com/ele-gall-ac-mineducation/projets/blob/master/recherche%20de%20d%C3%A9claration%20d%E2%80%99accessibilit%C3%A9%20depuis%20un%20URL.sh>
