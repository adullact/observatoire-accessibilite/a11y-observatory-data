#!/bin/bash

#set -o errexit
#################################################################################

TEMP=$(getopt -o cpt --long file: \
    -n 'javawrap' -- "$@")

    # SC2181: Check exit code directly with e.g. 'if mycmd;', not indirectly with $?.
    # For more information: https://www.shellcheck.net/wiki/SC2181
#    if [[ $? != 0 ]]; then
#        echo "Terminating..." >&2
#        exit 1
#    fi

usage() {
    echo "usage: ${0} --file <filePath>"
    echo ''
    echo '  --file   <filePath>   MANDATORY: URLs file (one URL per line).'
    echo ''
    exit 2
}

# Note the quotes around $TEMP: they are essential!
eval set -- "$TEMP"

declare FILE=
while true; do
    case "$1" in
    --file)
        FILE="$2"
        shift 2
        ;;
    --)
        shift
        break
        ;;
    *) break ;;
    esac
done

# Check mandatory options
if [[ "${FILE}" == "" ]]; then
    echo ''
    echo 'Mandatory option is missing'
    echo ''
    usage
fi

# Check existence of the file
if [ ! -f  "${FILE}" ]; then
    echo ''
    echo "File does not exist: [ ${FILE} ]"
    echo ''
    usage
fi

#########################################
# Create a new version
#########################################


function call() {
    mkdir -p log/
    URL_TO_AUDIT=$(echo ${1} | sed -e 's/^[[:space:]]*//')
    curl ${URL_TO_AUDIT}                  \
	--silent                          \
	--insecure                        \
	--request GET                     \
        --connect-timeout 5               \
	--header "curl-may-want-a-header" \
	--data @my.input.file             \
	--output log/site.output          \
	--write-out %{http_code}          \
             	> log/http.response.code  \
               2> log/error.messages
    CURL_CODE=$?
    HTTP_CODE=$(cat log/http.response.code)
    echo "CURL ${CURL_CODE} / HTTP ${HTTP_CODE} ---> ${URL_TO_AUDIT}"
    if [ -f  "log/http.response.code" ]; then
        rm "log/http.response.code"
    fi
    if [ -f  "log/error.messages" ]; then
        rm "log/error.messages"
    fi
    if [ -f  "log/site.output" ]; then
        rm "log/site.output"
    fi
}


function check_URLs() {
    echo  "${FILE}"
    while read LINE
    do
        URL=$(echo "${LINE}" | sed -e 's/^[[:space:]]*//')
        if [ "${URL}" != "" ]; then
            call "${URL}"
        fi
    done < "${FILE}"
}

##########################################
# Main tasks
##########################################
check_URLs

exit 0

}
