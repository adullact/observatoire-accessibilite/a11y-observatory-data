# AMUE

```bash
# First usage
##############################################

# Get Asqatasun Docker files
git clone git@gitlab.com:asqatasun/asqatasun-docker.git
mv asqatasun-docker git.asqatasun-docker
cd git.asqatasun-docker

cd 5.x/5-SNAPSHOT/all-5-SNAPSHOT_ubuntu-18.04/
cp -v .env.dist .env
vim .env

docker-compose build
docker-compose up

####################################

cd git.asqatasun-docker
cd 5.x/5-SNAPSHOT/all-5-SNAPSHOT_ubuntu-18.04/
docker-compose up
```

```bash
# First usage
##############################################

# Get Asqatasun Docker files
git clone git@gitlab.adullact.net:external-data/a11y-observatory-data.git
cd source-data/amue/
./asqatasun_load-audits_by-file-URLs.sh --file source/amue_websites.txt
```
