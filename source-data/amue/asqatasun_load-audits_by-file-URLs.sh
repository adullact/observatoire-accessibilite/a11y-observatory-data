#!/bin/bash

set -o errexit
#################################################################################
ASQA_USER="admin%40asqatasun.org"
ASQA_PASSWORD="myAsqaPassword"
PROJECT_ID="1"
API_PREFIX_URL="http://${ASQA_USER}:${ASQA_PASSWORD}@localhost:8081"
# API_URL="${API_PREFIX_URL}/api/v1/audit/run"
API_URL="${API_PREFIX_URL}/api/v0/audit/page/run"

REFERENTIAL="RGAA_4_0"
LEVEL="AA"
#################################################################################

TEMP=$(getopt -o cpt --long file:,tag:,tagPrefix: \
    -n 'javawrap' -- "$@")

    # SC2181: Check exit code directly with e.g. 'if mycmd;', not indirectly with $?.
    # For more information: https://www.shellcheck.net/wiki/SC2181
#    if [[ $? != 0 ]]; then
#        echo "Terminating..." >&2
#        exit 1
#    fi

TIME=$(date +%Hh%M)
DATE=$(date +%Y.%m.%d) 
usage() {
    echo "usage: ${0} --file <filePath> [-tagPrefix <tagPrefix>] [--tag <tagName>]"
    echo ""
    echo "  --file       <filePath>  MANDATORY - URLs file (one URL per line)."
    echo "  --tag        <tagName>   Asqatasun tag audit. Default: <fileName>_${DATE}-${TIME}"
    echo "  --tagPrefix  <tagPrefix> Prefix for Asqatasun tag audit. Default: <filePath>"
    echo ""
    exit 2
}

# Note the quotes around $TEMP: they are essential!
eval set -- "$TEMP"

declare FILE=
while true; do
    case "$1" in
    --file)
        FILE_PATH="$2"
        shift 2
        ;;
    --tagPrefix)
        TAG_PREFIX="$2"
        shift 2
        ;;
    --tag)
        TAG_NAME="$2"
        shift 2
        ;;
    --)
        shift
        break
        ;;
    *) break ;;
    esac
done

# Check mandatory options
if [[ "${FILE_PATH}" == "" ]]; then
    echo ''
    echo 'Mandatory option is missing'
    echo ''
    usage
fi

# Check existence of the file
if [ ! -f  "${FILE_PATH}" ]; then
    echo ''
    echo "File does not exist: [ ${FILE_PATH} ]"
    echo ''
    usage
fi
FILE_NAME=$(basename -- $FILE_PATH)

# Tag prefix option
if [[ "${TAG_PREFIX}" == "" ]]; then    
    TAG_PREFIX="${FILE_NAME}"
fi

# Tag name option
if [[ "${TAG_NAME}" == "" ]]; then
    TAG_NAME="${TAG_PREFIX}_${DATE}-${TIME}"
fi


#########################################
# Create a new version
#########################################


function call_api() {
    URL_TO_AUDIT=$(echo ${1} | sed -e 's/^[[:space:]]*//')
    curl -X POST \
         "${API_URL}"                                               \
         -H  "accept: */*"                                          \
         -H  "Content-Type: application/json"                       \
         -d "{                                                      \
                \"urls\": [    \"${URL_TO_AUDIT}\"  ],              \
                               \"referential\": \"${REFERENTIAL}\", \
                               \"level\": \"${LEVEL}\",             \
                               \"contractId\": ${PROJECT_ID},       \
                               \"tags\": [\"${TAG_NAME}\"]          \
             }"
    echo " - ${URL_TO_AUDIT}"
}



function asqatasun_audit_by_URLs_source_file() {
    echo "URL source file : ${FILE_PATH} ---> ${FILE_NAME}"
    echo "Asqatasun TAG   : ${TAG_NAME}"
    echo  "--------------------------------------"
    ######################################
    while read LINE
    do
        URL=$(echo "${LINE}" | sed -e 's/^[[:space:]]*//')
        if [ "${URL}" != "" ]; then
            call_api "${URL}"
            sleep 20
        fi
    done < "${FILE_PATH}"
    ######################################
    echo  "--------------------------------------"
    echo "URL source file : ${FILE_PATH}"
    echo "Asqatasun TAG   : ${TAG_NAME}"
}

##########################################
# Main tasks
##########################################
asqatasun_audit_by_URLs_source_file

exit 0

}
