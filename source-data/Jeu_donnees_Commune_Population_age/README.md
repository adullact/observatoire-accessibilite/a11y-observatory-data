# Création de la base de donnée de l'âge de la population des collectivités

Pour crée cette base de données il nous faudra :

- Fichier 1 : [Population-âge-sexe (INSEE)](https://www.insee.fr/fr/statistiques/1893204)
  - Onglet COM 2018
  - J'enregistre ce fichier sous format csv
  - Dans ce fichier, je vais prendre l'age de la population
- Fichier 2 : [Populations légales 2019 (INSEE)](https://www.insee.fr/fr/statistiques/6011070?sommaire=6011075)
  - Dans ce fichier, la population total nous interesse
- Fichir 3:  Avec la note Asqatasun

```python
# dictionnaire utilisé
import pandas as pd
import numpy as np
from datetime import datetime

# lire le fickier 1
df = pd.read_csv("pop-sexe-age-quinquennal6818.csv", delimiter=";", low_memory=False)

# Supprimer des colonnes
df1 = df.drop(columns=['De 0 à 4 ans\nHommes\nRP2018', 'De 0 à 4 ans\nFemmes\nRP2018',
                       'De 5 à 9 ans\nHommes\nRP2018', 'De 5 à 9 ans\nFemmes\nRP2018', 'De 10 à 14 ans\nHommes\nRP2018',
                       'De 10 à 14 ans\nFemmes\nRP2018', 'De 15 à 19 ans\nHommes\nRP2018',
                       'De 15 à 19 ans\nFemmes\nRP2018'])

# Supprimer des colonnes vides
df1 = df1.dropna()

# Convertir en fichier csv
df1.to_csv('Âge_population.csv')
```

Par la suite, on va faire des modifications sur **libre office**.

- Ajouter des colonnes (Sélectionner une colonne puis clic droit et insérer des colonnes après) pour faire la somme (
  =Somme('sélectionner les cases qu'on veut additionner)) :
  - Femme + homme agées de 20 à 40 ans
  - Femme + homme agées de 40 à 60 ans
  - Femme + homme agées de plus de 60 ans
    Pour reproduire la formule sur toute la ligne : mettre le cursus sur la case à bas à droite,
    un signe + va apparaître ensuite toucher la touche Ctrl et double cliquer sur le +
- Renommer les colonnes
- Enregistrer

Une fois les modifications faite on réintègre ce fichier sur **python notebook**.

```python
# Lire le fichier
df2 = pd.read_csv("Âge_population_.csv", delimiter=",", low_memory=False)

# Supprimer des colonnes
df3 = df2.drop(columns=['De 20 à 24 ans\nHommes\nRP2018', 'De 20 à 24 ans\nFemmes\nRP2018',
                        'De 25 à 29 ans\nHommes\nRP2018', 'De 25 à 29 ans\nFemmes\nRP2018',
                        'De 30 à 34 ans\nHommes\nRP2018',
                        'De 30 à 34 ans\nFemmes\nRP2018', 'De 35 à 39 ans\nHommes\nRP2018',
                        'De 35 à 39 ans\nFemmes\nRP2018',
                        'De 40 à 44 ans\nHommes\nRP2018', 'De 40 à 44 ans\nFemmes\nRP2018',
                        'De 45 à 49 ans\nHommes\nRP2018',
                        'De 45 à 49 ans\nFemmes\nRP2018', 'De 50 à 54 ans\nHommes\nRP2018',
                        'De 50 à 54 ans\nFemmes\nRP2018',
                        'De 55 à 59 ans\nHommes\nRP2018', 'De 55 à 59 ans\nFemmes\nRP2018',
                        'De 60 à 64 ans\nHommes\nRP2018',
                        'De 60 à 64 ans\nFemmes\nRP2018', 'De 65 à 69 ans\nHommes\nRP2018',
                        'De 65 à 69 ans\nFemmes\nRP2018',
                        'De 70 à 74 ans\nHommes\nRP2018', 'De 70 à 74 ans\nFemmes\nRP2018',
                        'De 75 à 79 ans\nHommes\nRP2018',
                        'De 75 à 79 ans\nFemmes\nRP2018', 'De 80 à 84 ans\nHommes\nRP2018',
                        'De 80 à 84 ans\nFemmes\nRP2018',
                        'De 85 à 89 ans\nHommes\nRP2018', 'De 85 à 89 ans\nFemmes\nRP2018',
                        'De 90 à 94 ans\nHommes\nRP2018',
                        'De 90 à 94 ans\nFemmes\nRP2018', '95 ans et plus\nHommes\nRP2018',
                        '95 ans et plus\nFemmes\nRP2018'])

# Pour faciliter la fusion, supprimer et renommer les colonnes
df3 = df3.drop(columns=['Département\nen géographie 2020', 'Commune'])
df3 = df3.rename(columns={'Code_commune': 'Code_Commune'})

# Ajouter un autre fichier 2
df4 = pd.read_csv("population2019.csv", delimiter=";", low_memory=False)

# Renommer les colonnes
df4 = df4.rename(columns={'Code commune': 'Code_Commune', 'Code région': 'Code_Region', 'Nom de la commune': 'Commune',
                          'Code département': 'Code_Dpt', 'Nom de la région': 'Region'})

# Fusionner les 2 fichiers
df5 = pd.merge(df4, df3)

# Supprimer les doublons
df5 = df5.drop_duplicates()

# Insérer le fichier qui contien la note Asqatasun 3
df6 = pd.read_csv("Asqatasun_occurrence_erreur_.csv", delimiter=",", low_memory=False)

# Supprimer les colonnes qu'on ne veut pas
df6 = df6.drop(columns=['Unnamed: 0', 'Population'])

# Fusionner les fichiers
df7 = pd.merge(df6, df5)

# Supprimer les doublons
df7 = df7.drop_duplicates()

# Convertir en csv
df7.to_csv('Audit_population_âge.csv')
```

![](Screenshot_pop_age.png)
