# Construction de la base de données des préfectures

Pour construire les données des préfectures, nous utilisons ces sources de données :

- Fichier 1 :
  [Annuaire de l’administration](http://etablissements-publics.api.gouv.fr/v3/organismes/prefecture)
- Fichier 2 :
  [Populations légales 2019 (INSEE)](https://www.insee.fr/fr/statistiques/6011070?sommaire=6011075)

Nous aurons deux données pour la préfecture :

La première : les préfectures sans les communes (103 préfectures) avec la population

```python
# Ouvrir les dictionnaires
import pandas as pd
import numpy as np
from datetime import datetime

# Lire le fichier 1 csv
df = pd.read_csv("prefecture.csv", delimiter=";", low_memory=False)

# Lire le fichier 2 csv
df1 = pd.read_csv("donnees_communes.csv", delimiter=";", low_memory=False)

# Renommer et supprimer des colonnes
df1 = df1.rename(columns={'CODREG': 'Code_Region', 'REG': 'Region', 'CODCOM': 'Code_Commune', 'COM': 'Commune',
                          'PTOT': 'Population_total', 'CODDEP': 'Code_Dpt'})
df1 = df1.drop(columns=['Code_Commune', 'CODARR', 'CODCAN'])

# Fusionner les 2 fichiers
df2 = pd.merge(df, df1)

# Supprimer les doublons
df2 = df2.drop_duplicates()

# Convertir en csv
df2.to_csv('Prefecture_pop_com.csv')
```

![](Screenshot_pop_com.png)

La seconde : les préfectures avec les communes (44658 lignes) avec la population, région et code département

```python
# Ouvrir les dictionnaires
import pandas as pd
import numpy as np
from datetime import datetime

# Supprimer les colonnes
df3 = df2.drop(columns=['Code_Commune', 'Commune', 'Code_Region', 'Region', 'Code_Dpt'])

# Grouper les colonnes
df4 = df3.groupby(['Prefecture_id', 'Code_INSEE', 'Préfecture_Nom', 'URL', 'Longitude', 'Latitude'])[
  'Population_total', 'PMUN', 'PCAP'].sum().reset_index()

# Convertir en csv
df4.to_csv('prefecture_population.csv')
```

![](Screenshot_prefecture_population.png)

## Résultat de la base de donnée préfecture

[Base de donnée préfecture](prefecture_population.csv)
