# Construction de la base de donnée de la population de plus de 60 ans

Pour créer la base de donnée population de plus de 60 ans, nous allons utiliser les fichiers :

- **Fichier 1** : [les URLs auditées](audits.log)
- **Fichier 2** : [Audit sans doublons](audits_sans_doublons.csv)
- **Fichier
  3
  ** : [Nombre d'occurrences d'erreurs](https://gitlab.adullact.net/adullact/observatoire-accessibilite/poc/a11y-observatory-tools/-/blob/49-ranger-les-fichiers-dans-un-dossier-organisation/documentation/Superset/User/3_Construction_graphes_et_donn%C3%A9e/Construction_des_donnees_en_fonction_du_nombre_doccurrences_derreurs.md)
- **Fichier 4** : [Mairie](http://etablissements-publics.api.gouv.fr/v3/organismes/mairie)
- **Fichier
  5** : [communes-departement-region](https://www.data.gouv.fr/fr/datasets/communes-de-france-base-des-codes-postaux/)
- **Fichier
  6
  ** : [Âge de la population](https://gitlab.adullact.net/adullact/observatoire-accessibilite/a11y-observatory-data/-/blob/main/source-data/Population_plus_60_ans/%C3%89tape%20de%20cr%C3%A9ation%20de%20la%20base%20de%20donn%C3%A9e%20de%20l'%C3%A2ge%20de%20la%20population.md)

Avant d'intégrer le fichier 1 dans Python, il faut tout d'abord mettre les urls sur le même format dans libre office :

```libreoffice
=SI(STXT(B2;1;11) = "http://www.";B2;SI(STXT(B2;1;12) = "https://www.";CONCAT("http://www.";STXT(B2;13;NBCAR(B2)))
;SI(STXT(B2;9;4) <> "www.";CONCAT("http://www.";STXT(B2;CHERCHE("//";B2)+2;NBCAR(B2)))))).
```

Ensuite dans Python, nous allons voir s'il y a des doublons.

```python
# Libraries utilisées
import pandas as pd
import numpy as np
from datetime import datetime

# Lire le fichier 1
df = pd.read_csv("audits.csv", delimiter=",", low_memory=False)

# Compter et énumérer les doublons
dups = df.pivot_table(index=['URL'], aggfunc='size')

# Convertir en fichier csv
dups.to_csv('dups.csv')
```

Grâce au fichier 'dups.csv', nous allons supprimer manuellement sur libre office tout les doublons (Voir fichier 2).

```python
# Lire le fichier 2 sans doublons
df1 = pd.read_csv("audits_sans_doublons.csv", delimiter=",", low_memory=False)

# Supprimer des colonnes
df1 = df1.drop(columns=['URLs'])

# Supprimer les doublons
df1 = df1.drop_duplicates()

# Lire le fichier 3
df2 = pd.read_csv("Nombre _Occurences_Erreurs.csv", delimiter=",", low_memory=False)

# Supprimer des colonnes
df2 = df2.drop(columns=['URLs', 'URL'])

# Fusionner le fichier 2 et 3
df3 = pd.merge(df1, df2)

# Lire le fichier 4
df4 = pd.read_csv("mairies_.csv", delimiter=",", low_memory=False)

# Supprimer une colonne
df4 = df4.drop(columns=['URLS'])

# Fusionner le fichier 4 avec les autres
df5 = pd.merge(df4, df3)

# Supprimer les doublons
df5 = df5.drop_duplicates()

# Supprimer les colonnes
df5 = df5.drop(columns=['Commune', 'Code_Commune'])

# Convertir la colonne en date et supprimer ce qu'on ne veut pas garder
df5['Date'] = pd.to_datetime(df5['Dt_Creation']).dt.date
df5['Time'] = pd.to_datetime(df5['Dt_Creation']).dt.time
df5 = df5.drop(columns=['Time', 'Dt_Creation'])

# Lire le fichier 5
df6 = pd.read_csv("communes-departement-region.csv", delimiter=",", low_memory=False)

# Supprimer et renommer les colonnes
df6 = df6.rename(columns={'code_commune_INSEE': 'Code_INSEE', 'latitude': 'Latitude', 'nom_commune_complet': 'Commune',
                          'longitude': 'Longitude', 'code_departement': 'Code_Dpt', 'nom_departement': 'Departement',
                          'code_region': 'Code_Region', 'nom_region': 'Region'})
df6 = df6.drop(columns=['code_postal', 'code_commune', 'nom_commune'])

# Supprimer les doublons
df6 = df6.drop_duplicates()

# Lire le fichier 6
df7 = pd.read_csv("Age_population.csv", delimiter=",", low_memory=False)

# Fusionner le fichier 5 et 6
df8 = pd.merge(df6, df7)

# Supprimer les doublons
df8 = df8.drop_duplicates()

# Fusionner
df9 = pd.merge(df5, df8)

# Supprimer les doublons
df9 = df9.drop_duplicates()

# Réorganiser les colonnes
df9 = df9[['Id_Audit', 'Id_Web_Resource', 'Code_INSEE', 'Code_Region', 'Region', 'Code_Dpt', 'Departement',
           'Code_Commune', 'Commune', 'Code_Postale', 'Latitude', 'Longitude', 'URL', 'Nb_Failed', 'Mark',
           'Mark_Letter',
           'Nombre_Occurence_Erreurs', 'Date', 'Population municipale', 'Population comptée à part',
           'Population totale',
           '20_39_ans', '40_59_ans', 'plus_60_ans']]

# Convertir en format csv
df9.to_csv('audit_communes.csv')
```

Par la suite, nous allons calculer la part de la population en pourcentage afin d'étudier ce jeu de donnée sous
plusieurs variations : La valeur supérieure à la médiane, le dernier quartile et le dernier décile.

```python
# Lire le fichier qu'on a préparer ci-dessous
df = pd.read_csv("audit_communes_.csv", delimiter=",", low_memory=False)

# Convertir en pourcentage
df['Population_60'] = (df['plus_60_ans'] * 100) / df['Population totale']

# Crée une colonne pour convertir le nombre d'occurence d'erreur en tranche d'erreur
Tranche_erreur = []
for value in df['Nombre_Occurence_Erreurs']:
  if value < 11:
    Tranche_erreur.append("Erreur 00 - 10")
  elif value > 10 and value < 51:
    Tranche_erreur.append("Erreur 10 - 50")
  elif value > 50 and value < 201:
    Tranche_erreur.append("Erreur 50 - 200")
  elif value > 200 and value < 501:
    Tranche_erreur.append("Erreurs 200 - 500")
  else:
    Tranche_erreur.append("Erreurs plus de 500")

df["Tranche_erreur"] = Tranche_erreur

# Calculer la médiane
Median = df['Population_60'].median()

# Calculer le quartile
df['Population_60'].quantile(0.75)

# Crée une colonne avec les valeurs du décile
df['Decile'] = pd.qcut(df['Population_60'], 10, labels=False)

# Convertir en format csv
df.to_csv('Commune_pop_age.csv')
```

![](Screeshot_pop_60ans.png)

Nous voulons regrouper les erreurs en deux parties :

- Erreur plus de 50
- Erreur moins de 50

Pour pouvoir faire ceci, dans libre office, nous allons copier coller sur une nouvelle colonne la colonne "
Tranche-erreur",
puis à l'aide de la commande recherche "ctrl+H " nous allons chercher et remplacer les valeurs.
ex : Erreur 50 - 200 sera Erreur plus de 50

## Résultat de la base de donnée population de plus de 60 ans

[Population de plus de 60 ans](Commune_pop_age.csv)
