# DINUM 250 Démarches

- Source : <https://observatoire.numerique.gouv.fr/observatoire/>
- Sur Data.gouv.fr <https://www.data.gouv.fr/fr/datasets/observatoire-de-la-qualite-des-demarches-en-ligne/> puis
  Fichier : `observatoire-edition-13-juillet-2022.ods`

![](Screenshot_250_démarche_Dinum.png)
