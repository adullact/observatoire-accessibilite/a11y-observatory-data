# Construction de la base de données du CROUS

Pour créer la base de données du CROUS, nous allons utiliser 3 fichiers :

- **Fichier 1** : [URL du CROUS](http://etablissements-publics.api.gouv.fr/v3/organismes/crous)
- [Convertir le fichier json](https://products.aspose.app/cells/conversion)
  - Dans LibreOffice, séparer les codes communes et les communes

```libreoffice
# Supprimer les premier caractere
=DROITE(A1;NBCAR(A1)-6)

# Garder les 1er caractere
=GAUCHE(T1;5)
```

- **Fichier 2** : [Liste des communes](https://www.insee.fr/fr/information/6051727)
   Fichier : `communes_2022.csv`
- **Fichier 3
  ** : [Étude supérieure](https://data.smartidf.services/explore/dataset/fr-esr-statistiques-sur-les-effectifs-d-etudiants-inscrits-par-etablissement/table/?sort=-annee_universitaire&refine.annee_universitaire=2021-22)
  - Fichier : Liste des régions en csv
  - Supprimer les colonnes que nous avons pas besoins

```python
# Library utiliser
import pandas as pd
import numpy as np

# Lire fichier 1
df = pd.read_csv("crous.csv", delimiter=",", low_memory=False)

# Supprimers les lignes vides
df = df[df['URL'].notna()]

# Supprimer les doublons
df = df.drop_duplicates()

# Lire le fichier 2
df1 = pd.read_csv("commune_2022.csv", delimiter=",", low_memory=False)

# Supprimer les lignes vides
df1 = df1[df1['Code_Region'].notna()]

# Fusionner les 2 fichiers
df2 = pd.merge(df, df1)

# Supprimer les doublons
df2 = df2.drop_duplicates()

# Lire le fichier 3
df3 = pd.read_csv("fr-esr-statistiques-sur-les-effectifs-d-etudiants-inscrits-par-etablissement.csv", delimiter=";",
                  low_memory=False)

# Supprimer des colonnes
df3 = df3.drop(columns=['etablissement_id_wikidata', 'etablissement_id_ror', 'etablissement_id_uai',
                        'effectif_sans_cpge', 'Code_Academie', 'Academie', 'etablissement_id_uucr',
                        'etablissement_uucr',
                        'Code_Commune', 'Commune', 'etablissement_id_paysage_actuel', 'etablissement_actuel_lib',
                        'Type_d’établissement', 'etablissement_id_paysage', 'etablissement_lib',
                        'effectif_total_sans_cpge'])

# Fusionner les fichiers
df4 = pd.merge(df2, df3)

# Grouper les colonnes
df5 = df4.groupby(['Longitude', 'Latitude', 'Properties_ID', 'Code_INSEE', 'Nom', 'Code_Postal',
                   'Commune', 'URL', 'Code_Region', 'Code_Dpt', 'Region', 'Departement'])[
  'effectif_total'].sum().reset_index()

# Supprimer les doublons
df5.drop_duplicates()

# Réorganiser les colonnes
df5 = df5[['Properties_ID', 'Code_INSEE', 'Nom', 'Region', 'Code_Region', 'Departement', 'Code_Dpt',
           'Code_Postal', 'Commune', 'Longitude', 'Latitude', 'URL', 'effectif_total']]

# Convertir en csv
df5.to_csv('crous_effectif.csv')
```

## Résultat de la base de donnée du CROUS

[Base de donnée du CROUS](crous_effectif.csv)
