# Construction de la base de données des sous-préfectures

Pour construire les données des sous-préfectures, nous utilisons ces sources de données :

- Fichier 1 :
  [sous-préfecture](http://etablissements-publics.api.gouv.fr/v3/organismes/sous_pref)
- Fichier 2 :
  [Populations légales 2019 (INSEE)](https://www.insee.fr/fr/statistiques/6011070?sommaire=6011075)

Nous aurons deux données pour la sous-préfecture :

La première : les sous-préfectures sans les communes et avec la population

```python
# Ouvrir les dictionnaires
import pandas as pd
import numpy as np
from datetime import datetime

# Lire le fichier 1 csv
df = pd.read_csv("sous_pref_.csv", delimiter=",", low_memory=False)

# Supprimer les colonnes
df = df.drop(columns=['Code_Postal'])

# Lire le fichier 2 csv
df1 = pd.read_csv("donnees_communes.csv", delimiter=";", low_memory=False)

# Renommer et supprimer des colonnes
df1 = df1.rename(columns={'CODREG': 'Code_Region', 'REG': 'Region', 'CODCOM': 'Code_Commune', 'COM': 'Commune',
                          'PTOT': 'Population_total', 'CODDEP': 'Code_Dpt'})
df1 = df1.drop(columns=['Code_Commune', 'CODARR', 'CODCAN'])

# Fusionner les 2 fichiers
df2 = pd.merge(df, df1)

# Supprimer les doublons
df2 = df2.drop_duplicates()

# Convertir en csv
df2.to_csv('sous_prefecture_commune_population.csv')
```

La seconde : les sous-préfectures avec les communes et la population, région et code département

```python
# Ouvrir les dictionnaires
import pandas as pd
import numpy as np
from datetime import datetime

# Supprimer les colonnes
df3 = df2.drop(columns=['Code_Commune', 'Commune'])

# Grouper les colonnes
df4 = df3.groupby(['Properties_id', 'Code_INSEE', 'Communes', 'Sous-Préfecture', 'URL'])[
  'Population_total', 'PMUN', 'PCAP'].sum().reset_index()

# Supprimer les doublons
df4 = df4.drop_duplicates()

# Convertir en csv
df4.to_csv('Sous_Préfecture_population.csv')
```

## Résultats de la base de donnée sous-préfecture

[Base de donnée sous-préfecture avec les communes et population](sous_prefecture_commune_population.csv)
[Base de donnée de la sous-préfecture sans les communes et avec la population](Sous_Préfecture_population.csv)
