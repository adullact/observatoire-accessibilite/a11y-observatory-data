# Construction de la base de données des caisses d'assurances retraites et de la santé au travail (CARSAT)

Pour créer la base de données des CARSAT, nous allons utiliser 2 fichiers :

- **Fichier 1** : [URL des CARSAT](http://etablissements-publics.api.gouv.fr/v3/organismes/carsat)
- [Convertir le fichier json](https://products.aspose.app/cells/conversion)
  - Dans LibreOffice, séparer les codes commune et les communes

```libreoffice
# Supprimer les premier caractere
=DROITE(A1;NBCAR(A1)-6)

# Garder les 1er caractere
=GAUCHE(T1;5)
```

- **Fichier 2** : [Liste des communes](https://www.insee.fr/fr/information/6051727)
  Fichier : `communes_2022.csv`
- **Fichier 3** : [Region](https://www.insee.fr/fr/information/5057840)
  Fichier : Liste des régions en csv
- **Fichier 4** : [Population des régions](https://www.insee.fr/fr/statistiques/1893198)
  Fichier : Estimation de population par région, sexe et grande classe d'âge - Années 1975 à 2022 en xlsx

```Python
# Library utiliser
import pandas as pd
import numpy as np

# Lire fichier 1
df = pd.read_csv("carsat.csv", delimiter=",", low_memory=False)

# Supprimers les lignes vides
df = df[df['URL'].notna()]

# Lire le fichier 2
df1 = pd.read_csv("commune_2022.csv", delimiter=",", low_memory=False)

# Supprimer les lignes vides
df1 = df1[df1['Code_Region'].notna()]

# Fusionner les 2 fichiers
df2 = pd.merge(df, df1)

# Lire le fichier 3
df3 = pd.read_csv("region2021.csv", delimiter=",", low_memory=False)

# Fusionner les fichiers
df4 = pd.merge(df2, df3)

# Lire le fichier 4
df5 = pd.read_csv("Population_region.csv", delimiter=",", low_memory=False)

# Fusionner les fichiers
df6 = pd.merge(df4, df5)

# Réorganiser les colonnes
df6 = df6[['Properties_ID', 'Code_INSEE', 'Nom', 'Code_Region', 'Region', 'Longitude', 'Latitude',
           'Code_Dpt', 'Code_Postal', 'Commune', 'URL', 'Code_Commune', 'Communes', '0 à 19 ans', '20 à 39 ans',
           '40 à 59 ans', '60 à 74 ans', '75 ans et plus', 'Population_Total']]

# Convertir en csv
df6.to_csv('CARSAT_Region_Population.csv')
```

## Résultat de la base de donnée des CARSAT

[Base de donnée des CARSAT](CARSAT_Region_Population.csv)
