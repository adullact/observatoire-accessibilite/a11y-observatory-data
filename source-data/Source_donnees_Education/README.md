# Éducation

## 1. Annuaire de l'éducation

see : [etalab/noms-de-domaine-organismes-publics #31 -
établissements scolaires publics](https://github.com/etalab/noms-de-domaine-organismes-publics/issues/31)

Automatisation possible de l'extraction avec l'API de l'annuaire de l'éducation :
<https://api.gouv.fr/documentation/api-annuaire-education>

Exemple curl :

```bash
curl -X 'GET' \
   'https://data.education.gouv.fr/api/v2/catalog/datasets/fr-en-annuaire-education/records?select=web%2Ccode_commune%2Cidentifiant_de_l_etablissement%2Csiren_siret&where=statut_public_prive%3D%22Public%22%20and%20web%20is%20not%20null&limit=10&offset=0' \
   -H 'accept: application/json'
```

- 8468 établissements scolaires publics avec une url de site web
- dont 7855 urls uniques
- donnant 6436 domaines uniques (sans scheme, sans path)

[data.education.gouv.fr/explore/dataset/fr-en-annuaire-education](https://data.education.gouv.fr/explore/dataset/fr-en-annuaire-education/table/?disjunctive.nom_etablissement&disjunctive.type_etablissement&disjunctive.appartenance_education_prioritaire&disjunctive.type_contrat_prive&disjunctive.code_type_contrat_prive&disjunctive.pial)

- [export CSV, JSON, fichiers géographiques](https://data.education.gouv.fr/explore/dataset/fr-en-annuaire-education/export/?disjunctive.nom_etablissement&disjunctive.type_etablissement&disjunctive.appartenance_education_prioritaire&disjunctive.type_contrat_prive&disjunctive.code_type_contrat_prive&disjunctive.pial)
- [API](https://data.education.gouv.fr/explore/dataset/fr-en-annuaire-education/api/?disjunctive.nom_etablissement&disjunctive.type_etablissement&disjunctive.appartenance_education_prioritaire&disjunctive.type_contrat_prive&disjunctive.code_type_contrat_prive&disjunctive.pial)

![](Screenshot_annuaire_education.png)

## 2. Principaux établissements d'enseignement supérieur (data.gouv)

- Source : <https://www.data.gouv.fr/fr/datasets/principaux-etablissements-denseignement-superieur/>
- Fichier : `fr-esr-principaux-etablissements-enseignement-superieur.csv`
  - Liste des écoles, Université avec leur statut, la localisation, url..)

![](Screenshot_enseignement_sup.png)

## 3. Liste des adhérents AMUE

**FR Public sector - Source "Adhérents de l'AMUE"** :

- Source :
  - <https://www.data.gouv.fr/fr/datasets/adherents-de-lamue/>
  - <https://www.amue.fr/presentation/annuaires/annuaire-des-adherents/>
- _Fichier_ : `liste-adherents-amue-102021.csv`

![](Screenshot_liste_amue.png)

## 4. Noms-de-domaine-organismes-publics

### FR Public sector - Source "Dépôt Etalab - Noms de domaine des organismes publics"

Github
repository  [Etalab - Noms de domaine des organismes publics](https://github.com/etalab/noms-de-domaine-organismes-publics)

- [scripts](https://github.com/etalab/noms-de-domaine-organismes-publics/tree/master/scripts)
  (import, checker, ...)
- script to extract URLs from **Banatic** data :
  [import-base-nationale-sur-les-intercommunalites.py](https://github.com/etalab/noms-de-domaine-organismes-publics/blob/master/scripts/import-base-nationale-sur-les-intercommunalites.py)
  - Voir : FR Public sector - Source "Base nationale sur l’intercommunalité"
    (Banatic, par interieur.gouv.fr)
- script to extract URLs from **Auracom - Listes des sites gouv.fr** data :
  [import-auracom-opendata.py](https://github.com/etalab/noms-de-domaine-organismes-publics/blob/master/scripts/import-auracom-opendata.py)
  - voir : FR Public sector - Source "Auracom - Listes des sites gouv.fr "
- [Wikidata - SPARQL request to extract 16099 URLs](https://github.com/etalab/noms-de-domaine-organismes-publics/issues/41)
  - Voir : FR Public sector - Source "Wikidata"
- [établissements scolaires publics](https://github.com/etalab/noms-de-domaine-organismes-publics/issues/31)
  - Voir : FR Public sector - Source "Annuaire de l'éducation" (data.education.gouv.fr)
- Etablissements-scolaires :
  <https://github.com/etalab/noms-de-domaine-organismes-publics/blob/master/sources/etablissements-scolaires.txt>
- Universites :
  <https://github.com/etalab/noms-de-domaine-organismes-publics/blob/master/sources/universites.txt>
