# Création du jeu de donnée Collèges (gérés par les départements)

Pour construire la base de données pour les collèges, nous allons utiliser le jeu de données
[annuaire de l'éducation](https://data.education.gouv.fr/explore/dataset/fr-en-annuaire-education).
Une fois sur le site sélectionner les filtres :

- Type_etablissement : Collège
- Secteur : Public
- Puis exporter en fichier CSV : Seulement les 5435 enregistrements sélectionnés

Avant d'intégrer le fichier dans python, il faut tout d'abord supprimer les colonnes dont nous
n'avons pas besoin, ensuite nous allons nettoyer les données en supprimant les valeurs manquantes.

```python
# Librarie utilisé
import pandas as pd
import numpy as np
from datetime import datetime

# Lire le fichier
df = pd.read_csv("Colleges.csv", delimiter=";", low_memory=False)

# Supprimer les lignes vides
df = df[df['URLs'].notna()]
df = df[df['Nombre_d_eleves'].notna()]

# Réorganiser l'ordre des colonnes
df = df[['Etablissement_ID', 'College', 'Region', 'Code_Region', 'Academie', 'Code_Academie', 'Departement',
         'Code_Dpt', 'Commune', 'Code_Commune', 'Code_Postal', 'Latitude', 'Longitude', 'URL', 'Nombre_d_eleves']]

# Convertir en csv
df1.to_csv('Lycée_.csv')
```

![](Screenshot_college.png)

## Résultat de base de donnée college

[Base de donnée college](cdg_pop_.csv)
