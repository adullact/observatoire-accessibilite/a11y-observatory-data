# Construction de la base de donnée Départements

Pour créer la base de données des départements, nous allons utiliser deux fichiers :

- Fichier 1 : [Conseils départementaux](https://etablissements-publics.api.gouv.fr/v3/organismes/cg)
  - [Convertir le fichier json](https://products.aspose.app/cells/conversion)
  - Liste des départements, code_insee, longitude, latitude et url
- Fichier 2 : [Population par département en 2022](https://www.insee.fr/fr/statistiques/1893198)
  - Donnée départementale en format xlsx
  - Liste des départements, code_département, tranche d'âge et population total

Avant d'intégrer les fichiers dans Python, il faut les modifier. Sur libre office, supprimer les colonnes qu'on ne veut
pas et convertir en fichier csv

```python
# Libraries utilisées
import pandas as pd
import numpy as np

# Lire fichier 1
df = pd.read_csv("Cg.csv", delimiter=",", low_memory=False)

# Supprimer une colonne
df1 = df.drop(columns=['Zonage_commune'])

# Supprimer les cases vides
df1 = df1[df1['URL'].notna()]

# Supprimer les doublons
df1.drop_duplicates()

# Lire fichier 2
df2 = pd.read_csv("Population_departement.csv", delimiter=",", low_memory=False)

# Fusionner les 2 tables
df3 = pd.merge(df1, df2)

# Supprimer les doublons
df3.drop_duplicates()

# Convertir en fichier csv
df3.to_csv('Departement_pop.csv')
```

![](Screenshot_departement.png)

## Résultat de la base de donnée département

[Base de donnée département](departement.csv)
