# URLs PK (2021.01.05)

- [source files](00_source-files/)
- certainly based
  on [Service-public.fr - Annuaire de l’administration - Base de données locales](https://www.data.gouv.fr/fr/datasets/service-public-fr-annuaire-de-l-administration-base-de-donnees-locales/)

identified bugs:

- invalid lines ---> #1
- duplicate URLs in the same file ---> #2
- duplicate URLs in several files ---> #4
- National URLs in a file related to a department ---> #3
