# Construction de la base de donnée CDG (Centre de gestion de la fonction publique territoriale)

Pour créer la base de donnée CDG, nous allons utiliser 2 fichiers :

- **Fichier 1** :
  [Centre de gestion de la fonction publique territoriale](https://etablissements-publics.api.gouv.fr/v3/organismes/cdg)
  - [Convertir le fichier json](https://products.aspose.app/cells/conversion)
  - Liste des cdg, code_insee, longitude, latitude et url
- **Fichier 2** : [Population par département en 2022](https://www.insee.fr/fr/statistiques/1893198)
  - Donnée départementale en format xlsx
  - Liste des départements, code_département, tranche d'âge et population total

Avant d'intégrer les fichiers dans python, il faut les modifier : dans LibreOffice, supprimer les colonnes qu'on ne veut
pas et convertir en fichier CSV.

```python
# Libraries utilisées
import pandas as pd
import numpy as np

# Lire fichier 1
df = pd.read_csv("cdg.csv", delimiter=",", low_memory=False)

# Supprimer une colonne
df1 = df.drop(columns=['Zonage_commune'])

# Supprimer les cases vides
df1 = df1[df1['URL'].notna()]

# Supprimer les doublons
df1.drop_duplicates()

# Lire fichier 2
df2 = pd.read_csv("Population_departemental.csv", delimiter=",", low_memory=False)

# Fusionner les 2 tables
df3 = pd.merge(df1, df2)

# Supprimer les doublons
df3.drop_duplicates()

# Réorganiser l'ordre des colonnes
df3 = df3[['Code_INSEE', 'Properties_ID', 'Code_Dpt', 'Departement', 'Nom', 'Commune', 'Code_Postal',
           'Adresse', 'Longitude', 'Latitude', 'URL', '0 à 19 ans', '20 à 39 ans', '40 à 59 ans',
           '60 à 74 ans', '75 ans et plus', 'Population_Total']]

# Convertir en fichier csv
df3.to_csv('cdg_pop.csv')
```

Une fois que les données sont fusionnées sur python, nous allons ajouter les 2 CDG à la main
"la grande et petite couronne de la région île-de-france".
Pour avoir les tranches d'âges et la population totale de ce dernier, nous allons additionner :

- Grande couronne de la région Île-de-France : les valeurs des départements 78, 91 et 95 du fichier 2
- Petite couronne de la région Île-de-France : les valeurs des départements 92,93 et 94 du fichier 2

![](Screenshot_cdg.png)

## Résultat de la base de donnée CDG

[Base de donnée CDG](cdg_pop_.csv)
