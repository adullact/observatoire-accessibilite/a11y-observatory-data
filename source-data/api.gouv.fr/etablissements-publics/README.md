# Établissements publics (api.gouv.fr)

## Informations

<https://api.gouv.fr/les-api/api_etablissements_publics>

Pour lister les mairies :

<http://etablissements-publics.api.gouv.fr/v3/organismes/mairie>

## Communes

Objectifs :

1. Récupérer le code INSEE, le nom et l'URL des communes
2. Exporter le résultat au format CSV

```shell
./Generer-Commune-codeInsee-nom-url.sh
```
