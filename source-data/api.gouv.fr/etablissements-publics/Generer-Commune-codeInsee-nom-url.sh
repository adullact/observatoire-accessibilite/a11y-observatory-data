#!/usr/bin/env bash

MY_DATE=$(date +%Y-%m-%d)
MAIRIES_JSON="/tmp/mairies.json"
MAIRIES_REDUCED_JSON="/tmp/mairies-reduced.json"
MAIRIES_REDUCED_CSV="/tmp/communes-${MY_DATE}.csv"
API_ETABLISSEMENTS_PUBLICS="http://etablissements-publics.api.gouv.fr/v3/organismes/mairie"

function retrieve_mairies() {
    curl --request GET -sL \
         --url "${API_ETABLISSEMENTS_PUBLICS}"\
         --output "${MAIRIES_JSON}"
}

# From all the available entries for a local gov, we only keep:
# - nom
# - url
# - codeInsee
function create_mairies_reduced() {
    TMP_FILE="/tmp/mairies-reduced-tmp"
    touch "${TMP_FILE}"

    # Keep only nom, url, codeInsee
    jq '[ .features[0][] | { nom: .properties.nom, url: .properties.url, codeInsee: .properties.codeInsee } ] | map(select(.url != null))' \
        "${MAIRIES_JSON}" > "${TMP_FILE}"

    # In "nom", remove "Mairie - " or "Mairie déléguée - "
    jq '.[].nom |= sub("^.*? - ";"")' \
        "${TMP_FILE}" > "${MAIRIES_REDUCED_JSON}"

    rm "${TMP_FILE}"
}

function export_to_CSV() {
    # We use JQ to convert JSON to CSV
    # See https://earthly.dev/blog/convert-to-from-json/#convert-json-to-csv-via-the-command-line-using-jq
    jq -r '(map(keys) | add | unique) as $cols | map(. as $row | $cols | map($row[.])) as $rows | $cols, $rows[] | @csv' \
    "${MAIRIES_REDUCED_JSON}" > "${MAIRIES_REDUCED_CSV}" \
    && echo "Data exported in CSV in ${MAIRIES_REDUCED_CSV}" \
    || echo "Error while exporting CSV data"
}

retrieve_mairies
create_mairies_reduced
export_to_CSV

