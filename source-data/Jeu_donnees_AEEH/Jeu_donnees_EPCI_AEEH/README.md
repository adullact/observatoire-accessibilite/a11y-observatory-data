# Construction de la base de donnée des Allocations d'éducation de l'enfant handicapé (AEEH) pour les EPCI

Pour créer la base de données des AEEH des EPCI, nous allons utiliser 2 fichiers :

- **Fichier 1** :
  [AAH par EPCI](http://data.caf.fr/dataset/nombre-d-enfants-couverts-par-l-allocation-d-education-de-l-enfant-handicape-aeeh-par-epci)
  - Fichier xlsx à télécharger : Nombre d’enfants couverts par l’AEEH
  - Dans libre office, nous allons supprimer les lignes dont nous n'avons pas besoin et nous choisirons
    comme date la plus récente et supprimerons les autres.
- **Fichier 2** : [EPCI](EPCI.csv)

```Python
# Library utiliser
import pandas as pd
import numpy as np

# Lire fichier 1
df = pd.read_csv("EJEPCI.csv", delimiter=";", low_memory=False)

# Supprimer les colonnes
df = df.drop(columns=['Lib_EPCI', 'Code_Dpt'])

# Lire le fichier 2
df1 = pd.read_csv("EPCI_dep_.csv", delimiter=",", low_memory=False)

# Supprimer les colonnes
df1 = df1.drop(columns=['nj_epci2022', 'fisc_epci2022', 'nb_com_2022', 'ptot_epci_2022', 'pmun_epci_2022'])

# Fusionner
df2 = pd.merge(df1, df)

# Réorganiser les colonnes
df2 = df2[['Code_INSEE', 'Id_EPCI', 'EPCI', 'Lib_EPCI', 'Code_Region', 'Région', 'Code_Dpt',
           'Département', 'Commune', 'Code_Commune', 'Latitude', 'Longitude', 'URL', 'NB_Allocataires', 'ALL_AEEH']]

# Convertir en csv
df2.to_csv("AEEH_EPCI_.csv")
```

## Résultat de la base de donnée des AEEH pour les EPCI

[Base de donnée des AEEH pour les EPCI](AEEH_EPCI_.csv)
