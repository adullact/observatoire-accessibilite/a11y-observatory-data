# Construction de la base de donnée des Allocations d'éducation de l'enfant handicapé (AEEH) pour les communes

Pour créer la base de données des AEEH des communes, nous allons utiliser deux 2 fichiers :

- **Fichier 1** :
  [AEEH_commune](http://data.caf.fr/dataset/nombre-d-enfants-couverts-par-l-allocation-d-education-de-l-enfant-handicape-aeeh-par-epci)
  - Fichier CSV à télécharger : Nombre d’enfants couverts par l’AEEH
  - Avant d'intégrer ce fichier dans Python, nous allons supprimer les lignes qui nous intéresse pas et nous choisirons
    comme date la plus récente et supprimer les autres.
- **Fichier 2** : [audit_commune](audit_communes_.csv)

```Python
# Library utiliser
import pandas as pd
import numpy as np

# Lire fichier 1
df = pd.read_csv("ENFANTAEEHCOM.csv", encoding='latin-1', delimiter=";", low_memory=False)

# Supprimer des colonnes
df = df.drop(columns=['Communes', 'Code_Dpt', 'DTREF'])

# Lire le fichier 2
df1 = pd.read_csv("audit_communes.csv", delimiter=",", low_memory=False)

# Supprimer les colonnes
df1 = df1.drop(columns=['Population municipale', 'Population comptée à part', 'Population totale',
                        '20_39_ans', '40_59_ans', 'plus_60_ans'])

# Fusionner les 2 fichiers
df2 = pd.merge(df1, df)

# Supprimer les colonnes vide
df2 = df2.dropna()

# Réorganiser les colonnes
df2 = df2[['Id_Audit', 'Id_Web_Resource', 'Code_INSEE', 'Code_Region', 'Region', 'Code_Dpt',
           'Departement', 'Code_Commune', 'Commune', 'Code_Postale', 'Latitude', 'Longitude', 'URL', 'Nb_Failed',
           'Mark', 'Mark_Letter', 'Nombre_Occurence_Erreurs', 'NB_Enfants', 'NB_enfant_AEEH', 'Date']]

# Convertir en csv
df2.to_csv('AEEH_audit_commune.csv')
```

## Résultat de la base de donnée des AEEH pour les collectivités

[Base de donnée des AEEH des collectivités](AEEH_audit_commune.csv)
