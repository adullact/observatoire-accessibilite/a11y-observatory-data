# Construction de la base de donnée des Allocations d'Éducation de l'Enfant Handicapé (AEEH) pour les départements

Pour créer la base de données des AEEH des départements, nous allons utiliser 2 fichiers :

- **Fichier 1** :
  [AAH pour la caf](http://data.caf.fr/dataset/foyers-allocataires-percevant-l-allocation-d-education-de-l-enfant-handicape-aeeh-par-caf)
  - Fichier csv à télécharger : AEEH - par caf
  - Avant d'intégrer ce fichier dans Python, nous allons supprimer les lignes qui nous intéresse pas et
    nous choisirons comme date la plus récente et supprimer les autres.
- **Fichier 3** : [Conseils départementaux](https://etablissements-publics.api.gouv.fr/v3/organismes/cg)
  - [Convertir le fichier json](https://products.aspose.app/cells/conversion)
  - Liste des départements, code_insee, longitude, latitude et url

```Python
# Library utiliser
import pandas as pd
import numpy as np

# Lire fichier 1
df = pd.read_csv("AEEHCAF.csv", encoding='latin', delimiter=";", low_memory=False)

# Lire le fichier 2
df3 = pd.read_csv("cg_dep.csv", delimiter=",", low_memory=False)

# Fusionner
df2 = pd.merge(df1, df)

# Réorganiser les colonnes
df2 = df2[['Code_INSEE', 'Code_Region', 'Code_Dpt', 'Departement', 'Commune', 'Code_Postal',
           'Adresse', 'Longitude', 'Latitude', 'URL', 'Effectif']]

# Convertir en csv
df4.to_csv('AEEH_dep.csv')
```

## Résultat de la base de donnée des AEEH pour les départements

[Base de donnée des AEEH pour les départements](AEEH_departement.csv)
