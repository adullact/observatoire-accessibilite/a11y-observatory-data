# Construction de la base de donnée SDIS (Service Départemental d'Incendie et de Secours)

Pour créer la base de données SDIS, nous allons utiliser 2 fichiers :

- **Fichier 1** :
  [Centre de gestion de la fonction publique territoriale](https://etablissements-publics.api.gouv.fr/v3/organismes/sdis)
  - [Convertir le fichier json](https://products.aspose.app/cells/conversion)
  - Liste des sdis, code_insee, longitude, latitude et url
- **Fichier 2** : [Population par département en 2022](https://www.insee.fr/fr/statistiques/1893198)
  - Donnée départementale en format xlsx
  - Liste des départements, code_département, tranche d'âge et population total

Avant d'intégrer les fichiers dans Python, il faut les modifier. Sur libre office, supprimer les colonnes dont on ne
veut pas et convertir en fichier CSV

```python
# Libraries utilisées
import pandas as pd
import numpy as np

# Lire fichier 1
df = pd.read_csv("sdis.csv", delimiter=",", low_memory=False)

# Supprimer une colonne
df1 = df.drop(columns=['Zonage_commune'])

# Supprimer les cases vides
df1 = df1[df1['URL'].notna()]

# Supprimer les doublons
df1.drop_duplicates()

# Lire fichier 2
df2 = pd.read_csv("Population_departemental.csv", delimiter=",", low_memory=False)

# Fusionner les 2 tables
df3 = pd.merge(df1, df2)

# Supprimer les doublons
df3.drop_duplicates()

# Réorganiser l'ordre des colonnes
df3 = df3[['Code_INSEE', 'Properties_ID', 'Pivot_Local' 'Code_Dpt', 'Departement', 'SDIS', 'Commune', 'Code_Postal',
           'Adresse', 'Longitude', 'Latitude', 'URL', '0 à 19 ans', '20 à 39 ans', '40 à 59 ans', '60 à 74 ans',
           '75 ans et plus', 'Population_Total']]

# Convertir en fichier csv
df3.to_csv('sdis_pop.csv')
```

Une fois que les données sont fusionnées sur Python, nous allons ajouter à la main
"Brigade des sapeurs-pompiers de Paris (BSPP) - pour les départements 75, 92, 93, 94".
Pour avoir les tranches d'âges et la population totale de ce dernier, nous allons ajouter :

- Brigade des sapeurs-pompiers de Paris (BSPP) : les valeurs des départements 75, 92, 93, 94 du fichier 2

![](Screenshot_sdis.png)

## Résultat de la base de donnée SDIS

[Base de donnée SDIS](sdis_pop.csv)
