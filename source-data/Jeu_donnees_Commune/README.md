# Construction de la base de donnée des communes

## Base de donnée des communes

Pour créer la base de données des communes, nous allons utiliser 6 fichiers.
Dans ces fichiers, nous allons mettre toutes les URLs sous la même forme :

```libreoffice
=SI(STXT(B2;1;11) = "http://www.";B2;SI(STXT(B2;1;12) = "https://www.";
CONCAT("http://www.";STXT(B2;13;NBCAR(B2)));SI(STXT(B2;9;4) <> "www.";
CONCAT("http://www.";STXT(B2;CHERCHE("//";B2)+2;NBCAR(B2))))))
```

- **Fichier 1** :
  [les 18000 Urls avant d'étre audité](https://gitlab.adullact.net/adullact/observatoire-accessibilite/poc/a11y-observatory-tools/-/blob/main/documentation/Superset/User/Comparaison_URL/audits.csv)
- **Fichier 2** : [Les audits sans les doublons](audits_sans_doublons.csv)
- **Fichier 3** : [Les Urls audités avec les nombre d'occurences](ASQATASUN_URL_Note_erreurs_occurrences.csv)
- **Fichier 4** : [Les données de la mairie](http://etablissements-publics.api.gouv.fr/v3/organismes/mairie)
  Pour convertir le fichier api format json à ods retenir ces liens :
  - <https://products.groupdocs.app/fr/conversion/json-to-ods>
  - <https://products.aspose.app/cells/conversion>
- **Fichier
  5** : [Commune-departement-region](https://www.data.gouv.fr/fr/datasets/communes-de-france-base-des-codes-postaux/)
- **Fichier 6** : [Âge de la population](Age_population.csv)

```python
# Libraries utilisées
import pandas as pd

# Lire le fichier 1
df = pd.read_csv("audits.csv", delimiter=",", low_memory=False)

# Voir les doublons
dups = df.pivot_table(index=['URL'], aggfunc='size')

# Convertir en csv
dups.to_csv('dups.csv')
# Une fois qu'on a les doublons on va supprimer manuellement sur libre office

# Lire le fichier 2
df1 = pd.read_csv("audits_sans_doublons.csv", delimiter=",", low_memory=False)

# Supprimer les colonnes
df1 = df1.drop(columns=['URLs'])

# Supprimer les doublons
df1 = df1.drop_duplicates()

# Lire le Fichier 3
df2 = pd.read_csv("Nombre _Occurences_Erreurs.csv", delimiter=",", low_memory=False)

# Supprimer les colonnes
df2 = df2.drop(columns=['URLs', 'URL'])

# Fusionner le fichier 2 et 3
df3 = pd.merge(df1, df2)

# Lire le fichier 4
df4 = pd.read_csv("mairies_.csv", delimiter=",", low_memory=False)

# Supprimer les colonnes
df4 = df4.drop(columns=['URLS'])

# Fusionner
df5 = pd.merge(df4, df3)

# Supprimer les doublons
df5 = df5.drop_duplicates()

# Supprimer des colonnes
df5 = df5.drop(columns=['Commune', 'Code_Commune'])

# Convertir le colonne qui comporte la date en datetime et supprimer le temps
df5['Date'] = pd.to_datetime(df5['Dt_Creation']).dt.date
df5['Time'] = pd.to_datetime(df5['Dt_Creation']).dt.time
df5 = df5.drop(columns=['Time', 'Dt_Creation'])

# Lire le fichier 5
df6 = pd.read_csv("communes-departement-region.csv", delimiter=",", low_memory=False)

# Renommer et supprimer des colonnes
df6 = df6.rename(columns={'code_commune_INSEE': 'Code_INSEE', 'latitude': 'Latitude',
                          'nom_commune_complet': 'Commune', 'longitude': 'Longitude', 'code_departement': 'Code_Dpt',
                          'nom_departement': 'Departement', 'code_region': 'Code_Region', 'nom_region': 'Region'})
df6 = df6.drop(columns=['code_postal', 'code_commune', 'nom_commune'])

# Lire le fichier 6
df7 = pd.read_csv("Age_population.csv", delimiter=",", low_memory=False)

# Fusionner
df8 = pd.merge(df6, df7)

# Supprimer les doublons
df8 = df8.drop_duplicates()

# Fusionner
df9 = pd.merge(df5, df8)

# Supprimer les doublons
df9 = df9.drop_duplicates()

# Réorganiser les colonnes
df9 = df9[['Id_Audit', 'Id_Web_Resource', 'Code_INSEE', 'Code_Region',
           'Region', 'Code_Dpt', 'Departement', 'Code_Commune', 'Commune', 'Code_Postale',
           'Latitude', 'Longitude', 'URL', 'Nb_Failed', 'Mark', 'Mark_Letter', 'Nombre_Occurence_Erreurs',
           'Date', 'Population municipale', 'Population comptée à part', 'Population totale', '20_39_ans',
           '40_59_ans', 'plus_60_ans']]

# Convertir en fichier csv
df9.to_csv('audit_communes.csv')
```

### Résultat de la base de donnée des communes

[Base de donnée des communes](audit_communes_.csv)

## Récupération et construction du jeu de données Asqatasun pour les communes

Dans cette partie, nous allons tout d'abord récupérer les audits des communes sur Superset,
puis nous allons construire le jeu de donnée sur python.

### Récupération des données Asqatasun des communes sur Supserset

Nous souhaitons avoir le nombre d'occurrences de "failed" et les notes Asqatasun sur un URL
pour un audit "completed". Pour cela, nous allons travailler avec les 5 tables : Audit, Audit_Tag,
Tag, Web_Ressource, Web_Ressource_statistics.

```sql
SELECT a.Id_Audit,
       a.Dt_Creation,
       wr.Url,
       wrs.Nb_Failed,
       wrs.Nb_Failed_Occurrences,
       wrs.Nb_Passed,
       wrs.Nb_Not_Tested,
       wrs.Nb_Na,
       a.Dt_Creation
FROM AUDIT a
       INNER JOIN AUDIT_TAG at on a.Id_Audit = at.Id_Audit
       INNER JOIN TAG ta on at.Id_Tag = ta.Id_Tag
       INNER JOIN WEB_RESOURCE wr ON wr.Id_Audit = a.Id_Audit
       INNER JOIN WEB_RESOURCE_STATISTICS wrs ON wrs.Id_Audit = a.Id_Audit

WHERE ta.Value = '2023-03-09-22h17_communes18k_firefox15_sleep0'
  AND a.Status = 'COMPLETED'
  AND wrs.Nb_Failed != 0

ORDER BY Url
```

![](Screenshot_Audit_Commune.png)

Une fois que nous avons la liste des audits des URLs, sur Superset, nous allons augmenter la "limit"
au maximum afin d'avoir tout les URLs, puis nous allons le télécharger en fichier CSV.
Ensuite, nous allons l'intégrer dans Python.

### Regrouper les données Asqatasun et les données des communes

Nous allons tout d'abord mettre les URLs sur le même format sur LibreOffice.

```libreoffice
=SI(STXT(B2;1;11) = "http://www.";B2;SI(STXT(B2;1;12)= "https://www.";CONCAT("http://www.";STXT(B2;13;NBCAR(B2)));SI(STXT(B2;9;4) <> "www.";CONCAT("http://www.";STXT(B2;CHERCHE("//";B2)+2;NBCAR(B2))))))
```

Ensuite, nous allons enregistrer le fichier en format CSV puis l'intégrer dans python.
Fichiers utilisés :

- **Fichier 1** : [Audit des communes](Audit_Commune_18k.csv)
- **Fichier 2** : [Base de donnée de la mairie](mairies_.csv)
- **Fichier 3** : [Population](Age_population.csv)

```python
# Librairies utilisées
import pandas as pd

# Lire le fichier 1
df = pd.read_csv("Audit_Commune_18k.csv", delimiter=",", low_memory=False)

# Supprimer une colonne
df1 = df.drop(columns=['Url', 'URLS'])

# Supprimer les doublons
df.drop_duplicates(subset="URL", keep='first', inplace=True)

# Convertir en fichier csv
df1.to_csv('Audit_commune_sans_doublons.csv')

# Lire le fichiers 2
df2 = pd.read_csv("mairies_.csv", delimiter=",", low_memory=False)

# Fusionner les 2 fichiers
df3 = pd.merge(df2, df1)

# Supprimer les doublons
df3.drop_duplicates(subset="URL", keep='first', inplace=True)

# Lire le fichier 3
df4 = pd.read_csv("population2019.csv", delimiter=",", low_memory=False)

# Supprimer les colonnes
df4 = df4.drop(columns=['Code_Commune', 'Commune'])

# Fusionner les colonnes
df5 = pd.merge(df3, df4)

# Supprimer les doublons
df5.drop_duplicates(subset="URL", keep='first', inplace=True)

# Convertir en fichier csv
df5.to_csv('Audit_commune_mairie.csv')
```

Une fois que nous avons constitué la base de données sur python,
nous allons ouvrir [le fichier 4](Audit_commune_sans_doublons.csv) puis dans une
colonne, nous allons ajouter les URLs du [fichier 5](Audit_commune_mairie.csv) pour pouvoir comparer
et voir les URLs qui n'ont pas été pris en compte pendant la fusion.
Pour pouvoir faire la comparaison des URLs et savoir combien de fois l’élément A apparait en colonne B,
si nous avons des égalités, on aura 1 sinon 0 :

```libreoffice
=NB.SI($B$2:$B$1046;$A$2:$A$1102)
```

Ensuite, nous allons aller dans 'Données - Autofiltre' et garder les valeurs 0.
Nous pourrons ainsi compléter à la main les valeurs manquantes ou supprimer les URLs
qui ne sont plus valides. Ensuite, sur une colonne, nous allons mettre en format
"ISO 3166-2 Codes" les départements : ="FR-"&F2. Ça va nous servir si nous voulons faire des cartes.
Une fois que les modifications sont faites on pourra faire les calculs statistiques

Nommons `Audit_commune_population.csv` le fichier résultant.

### Calcul statistique

Fichier utilisé :

- [**Fichier 1**](Audit_commune_population.csv) : le fichier que nous avons constitué ci-dessus

```python
import pandas as pd

# Lire le fichier
df = pd.read_csv("Audit_commune_population.csv", delimiter=",", low_memory=False)

# Convertir la colonne Dt_Creation en format date et heure et supprimer les
# colones qu'on ne veut pas
df['Date'] = pd.to_datetime(df['Dt_Creation']).dt.date
df['Time'] = pd.to_datetime(df['Dt_Creation']).dt.time
df = df.drop(columns=['Time', 'Dt_Creation'])

# Calcule de la médiane
Median = df['Nb_Failed_Occurrences'].median()

# Calcule de la moyenne
Moyenne = df['Nb_Failed_Occurrences'].mean()

# Calcule du quartile
Quartile = df['Nb_Failed_Occurrences'].quantile([0.25, 0.5, 0.75])

# Calcule du decile
df['Decile'] = pd.qcut(df['Nb_Failed_Occurrences'], 10, labels=False)

# Calcul de la moyenne pour chaque quartile
Quartile_1 = df.loc[df['Nb_Failed_Occurrences'] <= 13]
Quartile_1_moy = Quartile_1['Nb_Failed_Occurrences'].mean()
Quartile_2 = df.loc[(df['Nb_Failed_Occurrences'] > 13) & (df['Nb_Failed_Occurrences'] <= 25)]
Quartile_2_moy = Quartile_2['Nb_Failed_Occurrences'].mean()
Quartile_3 = df.loc[(df['Nb_Failed_Occurrences'] > 25) & (df['Nb_Failed_Occurrences'] <= 44)]
Quartile_3_moy = Quartile_3['Nb_Failed_Occurrences'].mean()
Quartile_4 = df.loc[(df['Nb_Failed_Occurrences'] > 44)]
Quartile_4_moy = Quartile_4['Nb_Failed_Occurrences'].mean()

# Calcul de la moyenne de la population dans chaque quartile
Quartile_1['Population totale'].sum()
Quartile_2['Population totale'].sum()
Quartile_3['Population totale'].sum()
Quartile_4['Population totale'].sum()

# Cacul du nombre de commune dans chaque quartile
Quartile_1['Commune'].count()
Quartile_2['Commune'].count()
Quartile_3['Commune'].count()
Quartile_4['Commune'].count()

# Crée une table avec les valeurs
data = {"Quartile": ['Q1', 'Q2', 'Q3', 'Q4'],
        "Moyen_Erreur": ['7.6', '19.2', '33.8', '94.15'],
        "Nombre_Ville": ['3967', '3964', '3598', '3838'],
        "Population_Total": ['12119552', '10726280', '10257780', '14555209'],
        "Date": ['2023-03-10', '2023-03-10', '2023-03-10', '2023-03-10']}
df1 = pd.DataFrame(data=data)

# Convertir en fichier csv
df1.to_csv('Quartile_Commune.csv')
df.to_csv('Audit_commune_population_decile.csv')
```

### Tranche des communes agrégées par département

Pour la réalisation des cartes, nous allons travailler sur une tranche de commune supérieure à 10000 habitants.

```python
# Librairies utilisées
import pandas as pd

# Lire le fichier
df= pd.read_csv("Audit_commune_population_decile_.csv", delimiter=",", low_memory=False)

# Choisir que les communes de plus de 10000 habitants
df1= df.loc[df['Population totale'] > 9999]

# Pour que le calcul de la médiane soit exact mettre les erreurs par ordre
df2=df1.sort_values(by=['Nb_Failed_Occurrences'], ascending=True)

# Agrégées les communes par département et calculer la médiane des erreurs
df2bis=df1.groupby(['Region', 'Code_Dpt', 'Iso Dpt', 'Pays', 'Code_Pays'])[['Nb_Failed_Occurrences']].median().reset_index()

# Mettre les non-conformités par ordre pour le calcul de la médiane
df3=df1.sort_values(by=['Nb_Failed'], ascending=True)

# Agrégées les communes par département et calculer la médiane des non_conformités
df3=df3.groupby(['Region', 'Code_Dpt', 'Iso Dpt', 'Pays', 'Code_Pays'])[['Nb_Failed']].median().reset_index()

# Fusionner les 2 tables
df4=pd.merge(df2, df3)

# Crée une colonne et faire les calculs pour les erreurs
Erreur_carte = []
for value in df4['Nb_Failed_Occurrences'] :
    if value < 10 :
       Erreur_carte.append(0)
    elif value >=10 and value < 30 :
        Erreur_carte.append(10)
    elif value >= 30 and value <= 60 :
        Erreur_carte.append(30)
    else :
        Erreur_carte.append(60)
df4["Erreur_carte"] = Erreur_carte

# Crée une colonne et faire les calculs pour les non-conformités
Non_conformité_carte = []
for value in df4['Nb_Failed'] :
    if value <= 3 :
       Non_conformité_carte.append(0)
    elif value >3 and value < 6 :
        Non_conformité_carte.append(3)
    else :
       Non_conformité_carte.append(6)
df4["Non_conformité_carte"] = Non_conformité_carte

# Convertir en csv
df4.to_csv('Tranche_Commune_agregees_departement_median.csv')
```

## Résultat de la base des données des communes avec les audits

- [Audit des communes par département et population](Audit_commune_population_decile.csv)
- [Donnée des quartiles](Quartile_Commune.csv)
- [Tranche des communes agrégées par département et calcul de la médianne](Tranche_Commune_agregees_departement_median.csv)
