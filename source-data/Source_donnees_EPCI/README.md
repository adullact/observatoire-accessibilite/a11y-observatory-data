# EPCI

## 1. Base des EPCI à fiscalité propre au 1ᵉʳ janvier 2022 (data.gouv)

- Source : <https://www.insee.fr/fr/information/2510634>
- Fichier : `Intercommunalite_Metropole_au_01-01-2022.xlsx`
  - onglet 1 : liste des EPCI (avec nombre de communes par EPCI)
  - onglet 2 : liste de toutes les communes, avec EPCI d'appartenance

![](Screenshot_EPCI_1.png)

## 2. Établissements - Publics - API

- Source : <http://etablissements-publics.api.gouv.fr/v3/organismes/epci>
  - liste des EPCI avec les communes, url et code INSEE

## 3. FR Public sector - API Geo ---> coordonnées géographique + population + EPCI

API Géo :

- coordonnées géographiques pour chaque commune
- population pour chaque commune ---> imparfait (exemple: Paris)
- communes qui composent un EPCI
- lister les EPCI d'un département

Documentation :

- <https://api.gouv.fr/documentation/api-geo>
- <https://geo.api.gouv.fr/decoupage-administratif>

Exemple :

- [Départements région Occitanie](https://geo.api.gouv.fr/regions/76/departements?fields=nom,code)
- [Communes pour le département 34](https://geo.api.gouv.fr/communes?codeDepartement=34&fields=nom,code,codesPostaux,centre,surface,contour,codeDepartement,departement,codeRegion,region,population&format=json&geometry=contour)
  (Geo: centre, contour, surface + Dpt + Region)
- [EPCI pour le département 34](https://geo.api.gouv.fr/epcis?codeDepartement=34&fields=nom,code,codesRegions,codesDepartements&format=json&geometry=centre)
- [Communes de l'EPCI "Montpellier Méditerranée Métropole"](https://geo.api.gouv.fr/communes?codeEpci=243400017)

Maj du 12/09/2022

> Mise à jour de l'API Géo - Découpage Administratifs : La recherche par EPCI est ajoutée, par exemple pour récupérer
> leur contour géographique, ou lister les EPCI d'un département.
>
> Autre exemple : connaître les communes qui composent un EPCI. Exemple avec la Communauté de Communes du Pays de
> Fayence <https://geo.api.gouv.fr/communes?codeEpci=200004802>
>
> Détails ici : <https://github.com/etalab/api-geo/releases/tag/v2.2.0>

Source : <https://twitter.com/APIGouv/status/1569631180365991936>

## 4. FR Public sector - Source "DGCL - Liste des EPCI à fiscalité propre" (collectivites-locales.gouv.fr)

Liste des EPCI à fiscalité propre de la DGCL :

- <https://www.collectivites-locales.gouv.fr/institutions/liste-et-composition-des-epci-fiscalite-propre>
- <https://www.collectivites-locales.gouv.fr/files/Accueil/DESL/2022/epcicom2022.xlsx>

Utilisés par :

- FR public sector - Source "Etalab - Données decoupage administratif COG avec codes postaux et population"
- FR Public sector - Source "Base nationale sur l’intercommunalité" (Banatic, par interieur.gouv.fr)

## 5. Base nationale sur les intercommunalités (data.gouv)

Source "Base nationale sur l’intercommunalité" (Banatic) --> <https://www.banatic.interieur.gouv.fr/>

- Identified via <https://github.com/etalab/noms-de-domaine-organismes-publics/issues/42>
- examples :
  - extract URLs from **Banatic** data:
    [etalab/noms-de-domaine-organismes-publics > `import-base-nationale-sur-les-intercommunalites.py`](https://github.com/etalab/noms-de-domaine-organismes-publics/blob/master/scripts/import-base-nationale-sur-les-intercommunalites.py)

### Data

#### Liste des groupements et des EPCI

Fichiers à télécharger ici :

- <https://www.banatic.interieur.gouv.fr/V5/fichiers-en-telechargement/fichiers-telech.php>
- <https://www.data.gouv.fr/fr/datasets/base-nationale-sur-les-intercommunalites/>

> Plusieurs fichiers sur l’intercommunalité décrivant la situation géographique, la nature juridique,
> la date de création, le périmètre, le profil financier, les compétences exercées

Fichiers France / par région / par département :

- Liste des groupements
- Coordonnées des groupements
- Compétences des groupements
- Périmètre des groupements
- Périmètre des EPCI à fiscalité propre

Le
fichier [métadonnées](https://www.banatic.interieur.gouv.fr/V5/ressources/documents/document_reference/Banatic_Metadonnees2013.xlsx)
pour la description des données disponibles :

- population du groupement
- communes associées au groupement
- site internet ---> données peu présentes (entre 300 à 600 URLs, pour 10 000 groupements)
- type de groupement :
  - METRO = Métropole
  - CU = Communauté urbaine
  - CA = Communauté d'agglomération
  - CC = Communauté de communes
  - SAN = Syndicat d'agglomération nouvelle
  - SIVU = Syndicat intercommunal à vocation unique
  - SIVOM = Syndicat intercommunal à vocation multiple
  - SMF = Syndicat mixte fermé
  - SMO = Syndicat mixte ouvert
  - POLEM = Pôle métropolitain

#### Contours des EPCI à fiscalité propre au format shapefile / SIG

<https://www.banatic.interieur.gouv.fr/V5/cartographie/cartographie.php>

### Autres données

#### Sites web ministériels / institutionnels / associations d'élus

Page HTML à scrapper : <https://www.banatic.interieur.gouv.fr/V5/ressources/liens-utiles.php>

- Sites ministériels
- Sites institutionnels
- Sites des associations d'élus
- Source : <https://www.data.gouv.fr/fr/datasets/base-nationale-sur-les-intercommunalites/>
