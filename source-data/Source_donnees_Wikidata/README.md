# Sources Wikidata

Items intéressants :

- [commune of France](https://www.wikidata.org/wiki/Q484170)

Propriétés intéressantes :

- [Official website](https://www.wikidata.org/wiki/Property:P856) /!\ il peut y a voir plusieurs valeurs
- Subsidiary // Part of
- [located in the administrative territorial entity](https://www.wikidata.org/wiki/Property:P131)
- [coordinate location](https://www.wikidata.org/wiki/Property:P625)
- [Population](https://www.wikidata.org/wiki/Property:P1082) /!\ il y a plusieurs valeurs (par année de recensement)
- [French public service directory ID](https://www.wikidata.org/wiki/Property:P6671) /!\ ce n'est PAS l'identifiant
  INSEE
- [INSEE municipality code](https://www.wikidata.org/wiki/Property:P374)
- [OpenStreetMap relation ID](https://www.wikidata.org/wiki/Property:P402)
- [SIREN number](https://www.wikidata.org/wiki/Property:P1616)

## SPARQL request on Wikidata

[query.wikidata.org](https://query.wikidata.org)

## Entités qui sont toutes instances de la classe « personne morale et organisme soumis au droit administratif (Q87712822)

[SPARQL request on query.wikidata.org](https://query.wikidata.org/#SELECT%20DISTINCT%20%3Fclass%20%3FclassLabel%20%3Fitem%20%3FitemLabel%20%0AWHERE%20%0A%7B%0A%20%20%20%20%3Fclass%20wdt%3AP279%2a%20wd%3AQ87712822%20.%0A%23%20%20%20%20%3Fsubclass%20wdt%3AP279%20%3Fclass%20.%0A%20%20%20%20%3Fitem%20wdt%3AP31%20%3Fclass%20.%0A%20%20%20%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22fr%22%20.%20%7D%0A%7D%0ALIMIT%20100000)

disponible :

- visualisation des données
- export JSON, CSV ---->
  [_Wikidata-entities_instances-of-Q87712822.csv](/uploads/a2f29d78cca10874985d2637e900a539/_Wikidata-entities_instances-of-Q87712822.csv)
- code Python, PHP, ... pour automatiser

```sql
SELECT DISTINCT ?class ?classLabel ?item ?itemLabel
WHERE
  {
  ? class wdt:P279* wd:Q87712822.
  # ?subclass wdt:P279 ? class.
  ?item wdt:P31 ? class.
  SERVICE wikibase: label { bd:serviceParam wikibase: language "fr".}
  }
LIMIT 100000
```

Fourni 47 000 entités qui sont toutes instances de la classe « personne morale et
organisme soumis au droit administratif (Q87712822) » (= une administration) ou de ses sous-classes (cf P279*)

Résultats catégorisés en :

- agence régionale de santé
- arrondissement municipal
- arrondissement municipal de Marseille
- autorité administrative ou publique indépendante
- caisse de crédit municipal
- collectivité d'outre-mer
- collectivité territoriale
- collectivité territoriale française à statut particulier
- collège
- collège expérimental
- communauté de communes
- commune de la Nouvelle-Calédonie
- commune de la Polynésie française
- commune française
- commune française à statut particulier
- Conseil départemental de l'accès au droit
- département
- département d'outre-mer
- district des Terres australes et antarctiques françaises
- établissement public administratif
- établissement public local culturel
- établissement public local d'enseignement
- groupement d'intérêt public (GIP)
- métropole
- ministère
- organisme consulaire
- pôle d'équilibre territorial et rural (PETR)
- pôle métropolitain
- province de la Nouvelle-Calédonie
- région
- service déconcentré de l'état à compétence (inter) départementale
- service départemental d'incendie et de secours (SDIS)
- subclassLabel
- syndicat intercommunal à vocation multiple
- syndicat intercommunal à vocation unique

## URLs d’entités qui sont toutes instances de la classe « personne morale et organisme soumis au droit administratif (Q87712822)

see : [extraction Wikidata : 16099 URLs](https://github.com/etalab/noms-de-domaine-organismes-publics/issues/41)

[SPARQL request on query.wikidata.org](https://query.wikidata.org/#SELECT%20DISTINCT%20%3Fclass%20%3FclassLabel%20%3Fitem%20%3FitemLabel%20%3Furl%0AWHERE%20%0A%7B%0A%20%20%20%20%3Fclass%20wdt%3AP279%2a%20wd%3AQ87712822%20.%0A%23%20%20%20%20%3Fsubclass%20wdt%3AP279%20%3Fclass%20.%0A%20%20%20%20%3Fitem%20wdt%3AP31%20%3Fclass%20.%0A%20%20%20%20%3Fitem%20wdt%3AP856%20%3Furl%0A%20%20%20%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22fr%22%20.%20%7D%0A%7D%0ALIMIT%20100000)

```sql
SELECT DISTINCT ?class ?classLabel ?item ?itemLabel ?url
WHERE
  {
  ? class wdt:P279* wd:Q87712822.
  ?item wdt:P31 ? class.
  ?item wdt:P856 ?url
  SERVICE wikibase: label { bd:serviceParam wikibase: language "fr".}
  }
LIMIT 100000
```

## POC Wikidata

<https://gitlab.adullact.net/adullact/observatoire-accessibilite/wikidata-poc/-/blob/main/wikidata_200.php>

POC en PHP :

- de manière récursive, il parcourt les régions (point d'entrée c'est l'ID Wikidata de chacune des régions)
- de manière récursive, il parcourt les départements
- pour chaque département, il parcourt les communes listées. C'est là où,
  il faudrait réécrire pour traiter les départements où les communes sont déclarées uniquement dans les cantons.

Le POC extrait 1/3 des communes et en y consacrant 15 jours, on peut extraire 100% des données.

- il faudrait réécrire pour traiter les départements où les communes sont déclarées uniquement dans les cantons.
- Les communes qui manquent sont dans les départements que le POC ne sait pas traiter. Mais rien de compliqué.
- actuellement, c'est un cache de 1.7 Go de données Json WikiData
- phase suivante : conception + écrire du code propre et maintenable (ce qui n'est pas le cas actuellement).

[000_global.csv](/uploads/7e3aa28fa380101a618ec3fce1e15d7c/000_global.csv)

13821 communes récupérées dont 5781 avec un site web

```csv
WikiData;INSEE;Région;Dpt;Département;Commune;Population;Date;Site web;
Q176415;01209;Auvergne-Rhône-Alpes;01;Ain;Léaz;758;2018;http://www.leaz.fr;
Q177568;01153;Auvergne-Rhône-Alpes;01;Ain;Échenevex;2092;2018;http://www.echenevex.fr;
Q188496;01034;Auvergne-Rhône-Alpes;01;Ain;Belley;9122;2018;http://www.belley.fr;
Q191856;01283;Auvergne-Rhône-Alpes;01;Ain;Oyonnax;22336;2018;http://www.oyonnax.fr;
Q192166;01004;Auvergne-Rhône-Alpes;01;Ain;Ambérieu-en-Bugey;14204;2018;http://www.ville-amberieuenbugey.fr;
Q193533;01007;Auvergne-Rhône-Alpes;01;Ain;Ambronay;2763;2018;http://www.ambronay.fr;
Q193812;01006;Auvergne-Rhône-Alpes;01;Ain;Ambléon;112;2018;;
Q193937;01269;Auvergne-Rhône-Alpes;01;Ain;Nantua;3410;2018;http://www.nantua-ville.fr;
Q193994;01022;Auvergne-Rhône-Alpes;01;Ain;Artemare;1244;2018;http://www.artemare.fr;
...
```
