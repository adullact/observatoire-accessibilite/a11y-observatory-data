# Comparaison entre les données des EPCI avec ceux de Wikidata

Pour comparer les données des EPCIs avec ceux de Wikidata, nous utilisons les sources de données suivantes :

- [Fichier 1](../../Jeu_donnée_EPCI/EPCI_dep.csv)
- [Fichier 2](double_epci_.csv)
- [Fichier 3](EPCI_dep_.csv)
- [Fichier 4](epci_wiki.csv)
- [Fichier 5](epci_wiki_.csv)

Requêtes utilisées pour extraire les données du fichier 4:

```sql
SELECT DISTINCT ?class ?EPCI ?classLabel ?item ?itemLabel ?url
WHERE
  {
  ? class wdt:P279* wd:Q3591564.
  ?item wdt:P1616 ?EPCI.
  ?item wdt:P31 ? class.
  ?item wdt:P856 ?url.
  ?item wdt:P1082 ?population.
  SERVICE wikibase: label { bd:serviceParam wikibase: language "fr".}
  }
LIMIT 100000
```

Avant de commencer la comparaison, nous devons d'abord mettre les URLs sur la même forme :

- Enlever les / à la fin des URLs (exemple) : `=SI(DROITE(E2;1)="/";STXT(E2;1;NBCAR(E2)-1) ;E2)`
- Changer les https en http et ajouter www s'il y en a pas. Exemple :

```libreoffice
=SI(STXT(F2;1;11) = "http://www.";F2;SI(STXT(F2;1;12) = "https://www.";CONCAT("http://www.";STXT(F2;13;NBCAR(F2)));SI(STXT(F2;9;4) <> "www.";CONCAT("http://www.";STXT(F2;CHERCHE("//";F2)+2;NBCAR(F2))))))
```

Pour faire la comparaison, nous allons procéder de deux façons :

- **1ère façon** : Nous allons fusionner les données sur l'URL (colonne en commun)
  --> Ce fichier va nous montrer que les valeurs en communs
- **2ème façon** : Nous allons fusionner les données sur la colonne EPCI, et nous allons faire en sortes que les
  colonnes URL n'ont pas le même nom pour faire la comparaison.

## Première façon

```python
# Librairies utilisées
import pandas as pd
import numpy as np

# Fichier 1 utiliser
df = pd.read_csv("EPCI_dep.csv", delimiter=",", low_memory=False)

# Afficher les doublons dans ce fichier
epci = df.pivot_table(index=['EPCI'], aggfunc='size')

# Convertir le fichier 2 des doublons en csv
epci.to_csv('double_epci_.csv')
```

À l'aide de ce fichier, nous pourrons supprimer manuellement les doublons soient ceux qui ont plus de 2. Une fois les
doublons supprimer, nous allons traiter les données dans Python

```python
# Lire le fichier 3
df = pd.read_csv("EPCI_dep.csv", delimiter=",", low_memory=False)

# Supprimer
df = df.drop(columns=['URLs'])

# Lire le fichier 4
df1 = pd.read_csv("epci_wiki.csv", delimiter=",", low_memory=False)

# Supprimer les colonnes
df1 = df1.drop(columns=['Lib_EPCI', 'URLs', 'URLS'])

# Pareil que pour le 1er fichier on vérifier les doublons et on converti en csv
doublons = df1.pivot_table(index=['EPCI'], aggfunc='size')
doublons.to_csv('double_epci_wiki.csv')

# Une fois que nous avons supprimer les doublons manuellement nous intégrons le fichier 5
df2 = pd.read_csv("epci_wiki_.csv", delimiter=",", low_memory=False)

# Supprimer les colonnes
df2 = df2.drop(columns=['Lib_EPCI', 'URLs', 'URLS', 'EPCI'])

# Fusionner les dataframes
df3 = pd.merge(df, df2)

# Supprimer les colonnes
df3.drop_duplicates()

# Réorganiser l'ordre des colonnes
df3 = df3[['Class', 'EPCI', 'Wiki_id', 'Code_INSEE', 'Id_EPCI', 'Lib_EPCI', 'Région', 'Code_Region',
           'Département', 'Code_Dpt', 'Commune', 'Code_Commune', 'Latitude', 'Longitude', 'URLs', 'URL',
           'nj_epci2022', 'fisc_epci2022', 'nb_com_2022', 'pmun_epci_2022', 'ptot_epci_2022', 'Population']]

# Convertir en fichier csv
df3.to_csv('epci_wiki_comp.csv')
```

[Résultat de la 1er façon de comparais](epci_wiki_comp.csv)

## Deuxième façon

```python
# Librairies utilisées
import pandas as pd
import numpy as np

# Lire le 2eme fichier
df = pd.read_csv("EPCI_dep_.csv", delimiter=",", low_memory=False)

# Supprimer et renommer les colonnes
df = df.drop(columns=['URLs'])
df = df.rename(columns={'URL': 'URLs'})

# Lire le fichier 5
df1 = pd.read_csv("epci_wiki_.csv", delimiter=",", low_memory=False)

# Supprimer les colonnes
df1 = df1.drop(columns=['Lib_EPCI', 'URLs', 'URLS'])

# Fusionner les dataframes
df3 = pd.merge(df, df2)

# Supprimer les doublons
df3.drop_duplicates()

# Réorganiser les colonnes
df3 = df3[['Class', 'EPCI', 'Wiki_id', 'Code_INSEE', 'Id_EPCI', 'Lib_EPCI', 'Région', 'Code_Region',
           'Département', 'Code_Dpt', 'Commune', 'Code_Commune', 'Latitude', 'Longitude', 'URLs', 'URL',
           'nj_epci2022', 'fisc_epci2022', 'nb_com_2022', 'pmun_epci_2022', 'ptot_epci_2022', 'Population']]

# Convertir en fichier csv
df3.to_csv('comparaison_wiki_epci.csv')
```

[Résultat de la 2ème façon de comparaison](comparaison_wiki_epci.csv)

La comparaison pour cette partie se fait manuellement une fois que nous avons mit les deux URLs côte à côte.

## Conclusion

Après avoir comparé de deux façons les résultats, nous remarquons que les données récoltées auparavant à l'aide de l'API
`api.gouv.fr` ou wikidata ont des résultats qui ne sont pas mis à jour donc pour avoir des données à jours il vaut mieux
combiner les 2.
