# Comparaison entre les données des communes avec ceux de Wikidata

Pour comparer les données des mairies avec ceux de Wikidata, nous utilisons les sources de données suivantes :

- [Fichier 1](mairies.csv)
- [Fichier 2](Commune_wiki.csv) :

Requêtes utilisées pour extraire les données du fichier 2:

```sql
SELECT DISTINCT ?class ?classLabel ?item ?itemLabel ?url
WHERE
  {
  ? class wdt:P279* wd:Q484170.
  ?item wdt:P31 ? class.
  ?item wdt:P856 ?url.
  SERVICE wikibase: label { bd:serviceParam wikibase: language "fr".}
  }
LIMIT 100000
```

Avant de commencer la comparaison, nous devons d'abord mettre les URLs sur la même forme :

- Enlever les / à la fin des URLs (exemple) : `=SI(DROITE(E2;1)="/";STXT(E2;1;NBCAR(E2)-1) ;E2)`
- Changer les https en http et ajouter www s'il y en a pas. Exemple :

```libreoffice
=SI(STXT(F2;1;11) = "http://www.";F2;SI(STXT(F2;1;12) = "https://www.";CONCAT("http://www.";STXT(F2;13;NBCAR(F2)));SI(STXT(F2;9;4) <> "www.";CONCAT("http://www.";STXT(F2;CHERCHE("//";F2)+2;NBCAR(F2))))))
```

Une fois les URLs modifiées, nous allons procéder à la comparaison avec python.

```python
# Librairies utilisées
import pandas as pd
import numpy as np

# Lire le 1er fichier
df = pd.read_csv("mairies.csv", delimiter=",", low_memory=False)

# Lire le fichier 2
df1 = pd.read_csv("Commune_wiki.csv", delimiter=",", low_memory=False)

# Fusionner les dataframes
df2 = pd.merge(df1, df)

# Supprimer les doublons
df2 = df2.drop_duplicates()

# Réorganiser les colonnes
df2 = df2[['Wiki_ID', 'Code_INSEE', 'Wiki_ID_Commune', 'Code_Commune', 'Code_Postale', 'Commune', 'url', 'URL']]

# Convertir en fichier csv
df2.to_csv('comparaison_wiki.csv')
```

[Résultat de la comparaison](comparaison_wiki.csv)

La comparaison pour cette partie se fait manuellement une fois que nous avons mit les deux URLs côte à côte.

## Conclusion

Après avoir comparé les résultats, bien que les données du Wikidata soient complètes, nous remarquons que tout n'est pas
à jour donc il vaut mieux combiner les deux données.
