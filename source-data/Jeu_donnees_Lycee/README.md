# Création du jeu de donnée lycées (gérés par les régions)

Pour construire la base de donnée pour les lycées, nous allons utiliser le fichier
[annuaire de l'éducation](https://data.education.gouv.fr/explore/dataset/fr-en-annuaire-education).
Une fois sur le site sélectionner les filtres :

- Type_etablissement :  Lycée
- Secteur : Public
- Puis exporter en fichirr csv : Seulement les 3483 enregistrements sélectionnés

Avant d'intégrer le fichier dans python, il faut tout d'abord supprimer les colonnes dont nous
n'avons pas besoin, ensuite nous allons nettoyer les données en supprimant les valeurs manquantes.

```python
# Librarie utilisé
import pandas as pd
import numpy as np
from datetime import datetime

# Lire le fichier
df = pd.read_csv("Lycée.csv", delimiter=";", low_memory=False)

# Supprimer les lignes vides
df1 = df1[df['URLs'].notna()]
df1 = df1[df['Nombre_d_eleves'].notna()]

# Convertir en csv
df1.to_csv('Lycée_.csv')
```

![](Screenshot_lycee.png)

## Résultat de la base de donée Université

[Base de donnée Lycée](Lycée_.csv)
