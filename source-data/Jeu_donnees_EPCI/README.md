# Construction de la base de donnée EPCI

## Base de donnée EPCI

Pour construire les données des EPCI, nous utilisons les sources de données suivantes :

- **Fichier 1** :
  [Etablissements - Publics - API](http://etablissements-publics.api.gouv.fr/v3/organismes/epci)
  - J'ai converti cette api (fichier json) sur 'Excel'
  - Pour la conversion de json à csv retenir ces liens :
    - <https://www.npmjs.com/package/json-2-csv>
    - <https://www.npmjs.com/package/json2csv>
- **Fichier 2** :
  [Base des EPCI à fiscalité propre au 1ᵉʳ janvier 2022 (data.gouv)](https://www.insee.fr/fr/information/2510634)
  --> onglet 2
  - Ce fichier nous permetra surtout de changer le nom des epci du fichier 1
- **Fichier 4** :
  [Pour les départements et régions](https://www.data.gouv.fr/fr/datasets/communes-de-france-base-des-codes-postaux/)
- **Fichier 5** :
  [Pour la population des EPCI](https://www.collectivites-locales.gouv.fr/institutions/liste-et-composition-des-epci-fiscalite-propre)
  - Liste des EPCI au 1er janvier 2022 (xls)
- **Fichier 6** :
  [FR Public sector - Source "DGCL - Liste des EPCI à fiscalité propre"](https://www.collectivites-locales.gouv.fr/files/Accueil/DESL/2022/epcicom2022.xlsx)

On aura 2 données EPCI :

- **le 1er** : les EPCI sans les communes (1253 EPCI) avec leur population, département et région
- **le 2ème** : Les EPCI avec les communes (32461 communes) avec la population par communes

```python
# Ouvrir des dictionnaires
import pandas as pd
import numpy as np
from datetime import datetime

# Lire le fichier 1
df = pd.read_csv("EPCI_.csv", delimiter=",", low_memory=False)

# Supprimer les alertes qui vont se réperter sur le fichier suivant
df = df.drop(columns=['Lib_EPCI', 'Adresses', 'Commune'])
# -> on supprime la colonne 'Lib_EPCI' car le nom des epci est différentes que les autres

# Lire le fichier 2
df1 = pd.read_excel("Intercommunalite_Metropole_au_01-01-2022.xlsx", header=5)

# Renommer et suprimer les colonnes qu'on a pas besoin
df1 = df1.rename(columns={'LIBEPCI': 'Lib_EPCI', 'DEP': 'Code_Dpt', 'REG': 'Code_Region'})
df1 = df1.drop(columns=['CODGEO', 'LIBGEO'])

# Fusionner les 2 tables
df2 = pd.merge(df, df1)  # -> la fusion se fera avec la colonne 'Code_Commune'

# Imprimer en format CSV
df.to_csv('EPCI.csv')  # --> fichier 3
```

![](Screenshot_EPCI.png)

Intégration des départements, régions et de la population des EPCI . Il faut utiliser :

- **Fichier 3**
- **Fichier 4**
- **Fichier 5**

```python
# Lire le fichier 3
df = pd.read_csv("EPCI_1.csv", delimiter=",", low_memory=False)

# Supprimer les colonnes
df = df.drop(columns=['Longitude', 'Latitude', 'Code_Postal'])

# Supprimer les doublons
df = df.drop_duplicates()

# Lire le fichier 4
df1 = pd.read_csv("communes-departement-region.csv", delimiter=",", low_memory=False)

# Supprimer les colonnes
df1 = df1.drop(columns=['code_postal', 'code_commune', 'nom_commune', 'nom_commune_complet'])

# Renommer les colonnes
df1 = df1.rename(columns={'code_commune_INSEE': 'Code_INSEE', 'code_departement': 'Code_Dpt',
                          'nom_region': 'Région', 'nom_departement': 'Département', 'code_region': 'Code_Région',
                          'latitude': 'Latitude', 'longitude': 'Longitude'})

# Fusionner les 2 tables
df2 = pd.merge(df, df1)

# Supprimer les doublons
df2 = df2.drop_duplicates()

# Lire le fichier 5
df3 = pd.read_excel("epcisanscom2022.xlsx", header=0)

# Supprimer et renommer les doublons
df3 = df3.drop(columns=['dep_epci', 'nom_complet'])
df3 = df3.rename(columns={'siren_epci': 'EPCI'})

# Fusionner les tables
df4 = pd.merge(df2, df3)

# Convertir en csv
df4.to_csv('EPCI_dep.csv')
```

![](Screenshot_EPCI_dpt.png)

Dans la suite, on va intégrer la population par commune dans le fichier 3.
Pour cela, on va utiliser le fichier 6.

```python
import pandas as pd
import numpy as np
from datetime import datetime

# Lire le fichier précèdent
df = pd.read_csv("EPCI.csv", delimiter=",", low_memory=False)

# Supprimer les colonnes qu'on ne veut pas
df = df.drop(columns=['Commune', 'Code_INSEE', 'EPCI', 'Code_Commune'])

# Lire le fichier 3
df1 = pd.read_excel("epcicom2022.xlsx", header=0)

# Renommer et supprimer les colonnes
df1 = df1.drop(columns=['dept', 'dep_com'])
df1 = df1.rename(columns={'siren': 'Siren', 'siren_membre': 'Siren_com', 'raison_sociale': 'Lib_EPCI',
                          'nature_juridique': 'Nature_juridique', 'mode_financ': 'Mode_financ',
                          'total_pop_tot': 'Pop_epsi_tot',
                          'total_pop_mun': 'Pop_mun_tot', 'nom_membre': 'Communes', 'total_pop_tot': 'EPCI_pop_tot',
                          'insee': 'Code_INSEE'})

# Fusionner les 2 tables
df2 = pd.merge(df1, df)

# Convertir en csv
df2.to_csv('EPCI_population.csv')
```

![](Screenshot_EPCI_avec_ses_communes_et_population_de_chaque_commune.png)

### Résultat de la base de donnée EPCI

[Base de donnée EPCI](EPCI_dep_.csv)

## Récupération et construction du jeu de données Asqatasun pour les EPCI

Dans cette partie, nous allons tout d'abord récupérer les audits des EPCI sur Superset,
puis nous allons construire le jeu de donnée sur python.

### Récupérer le fichier les données Asqatasun avec Superset

Nous souhaitons avoir le nombre d'occurrence de "failed" et les notes Asqatasun sur un URL
pour un audit "completed". Pour cela, nous allons travailler avec les 5 tables: Audit, Audit_Tag,
Tag, Web_Ressource, Web_Ressource_statistics.

```sql
SELECT
  a.Id_Audit,
  a.Dt_Creation,
  wr.Url,
  wrs.Nb_Failed,
  wrs.Nb_Failed_Occurrences,
  wrs.Mark,
  (CASE
  WHEN Mark > 99 THEN "A"
  WHEN Mark > 90 THEN "B"
  WHEN Mark > 85 THEN "C"
  WHEN Mark > 75 THEN "D"
  WHEN Mark > 60 THEN "E"
  ELSE "F" as Mark_Letter
  END) as Mark_Letter
FROM  AUDIT  a
INNER JOIN AUDIT_TAG at on a.Id_Audit = at.Id_Audit
INNER JOIN TAG t on at.Id_Tag = t.Id_Tag
INNER JOIN WEB_RESOURCE wr ON wr.Id_Audit = a.Id_Audit
INNER JOIN WEB_RESOURCE_STATISTICS wrs ON wrs.Id_Audit = a.Id_Audit

WHERE
  t.Value = '2023-01-25_epci_18h42_firefox5_sleeps'
  AND a.Status = 'COMPLETED'
  AND wrs.Nb_Failed != 0

ORDER BY Url  ASC;
```

![](Screenshot_Audit_EPCI.png)

Une fois que nous avons la liste des audits des URLs, sur Superset, nous allons augmenter la "limit"
au maximum afin d'avoir tout les URLs, puis nous allons le télécharger en fichier CSV.
Ensuite, nous allons l'intégrer dans python.

### Regrouper les données Asqatasun et les données des EPCI

Nous allons tout d'abord mettre les URLs sur le même format sur LibreOffice.

```libreoffice
=SI(STXT(B2;1;11) = "http://www.";B2;SI(STXT(B2;1;12)= "https://www.";CONCAT("http://www.";STXT(B2;13;NBCAR(B2)));SI(STXT(B2;9;4) <> "www.";CONCAT("http://www.";STXT(B2;CHERCHE("//";B2)+2;NBCAR(B2))))))
```

Ensuite, nous allons enregistrer le fichier en format CSV puis l'intégrer dans python.
Fichier utilisés :

- **Fichier 1** : [Audit des EPCI](EPCI_Audit.csv)
- **Fichier 2** : [Base de donnée des EPCI](EPCI_dep_.csv)

```python
# Librairies utiliser
import pandas as pd
import numpy as np
from datetime import datetime

# Lire le fichier 1
df= pd.read_csv("EPCI_Audit.csv", delimiter=",", low_memory=False)

# Supprimer les doublons
df.drop_duplicates(subset ="URL", keep = 'first', inplace=True)

# Convertir ce fichier en csv
df.to_csv('EPCI_Audit_sans_doublons.csv')

# Lire le fichier 2
df1= pd.read_csv("EPCI_dep_.csv", delimiter=",", low_memory=False)

# Fusionner les 2 fichiers
df2=pd.merge(df, df1)

# Convertir le fichier en format CSV
df2.to_csv('EPCI_Audit_Population.csv')
```

Une fois que nous avons constituer la base de données sur python,
nous allons ouvrir le fichier "EPCI_Audit_sans_doublons.csv" puis dans une colonne nous allons ajouter
les URLs du fichier "EPCI_Audit_Population.csv" pour pouvoir comparer et voir les URLs qui n'ont pas été pris
en compte pendant la fusion.
Pour pouvoir faire la comparaison des URLs et savoir combien de fois l’élément A apparait en colonne B,
si nous avons des égalités on aura 1 sinon 0 :

```libreoffice
=NB.SI($B$2:$B$1046;$A$2:$A$1102)
```

Ensuite, nous allons aller dans 'Données - Autofiltre' et garder les valeurs 0. Nous pourrons ainsi
compléter à la main les valeurs manquantes ou supprimer les URLs qui ne sont plus valides.
Sur une colonne, nous allons mettre en format "ISO 3166-2 Codes" les départements : ="FR-"&F2.
Ça va nous servir si nous voulons faire des cartes.

### Ajouter les nombres de passed, Na, les non-testée et les Nmi

Fichier utilisée :

- [**Fichier 1**](EPCI_audit_passed_na.csv) : Audit des EPCI avec le nombre des passed, Na..
- [**Fichier 2**](EPCI_Audit_population_decile_.csv) : Le fichier que nous avons constituer ci-dessus

```python
# Librairies utilisés
import pandas as pd
import numpy as np
from datetime import datetime

#Lire le Fichier 1
df= pd.read_csv("EPCI_audit_passed_na.csv", delimiter=",", low_memory=False)

# Supprimer les colonnes que nous avons pas besoin
df=df.drop(columns=['Url', 'Dt_Creation'])

# Lire le fichier 2
df1= pd.read_csv("EPCI_Audit_population_decile_.csv", delimiter=",", low_memory=False)

# Fusionner les 2 fichiers
df2=pd.merge(df1, df)

# Convertir en fichier csv
df2.to_csv('EPCI_POP_AUDIT_PASSED_NA_NON_TESTER.CSV')
```

Ensuite nous allons sur le fichier csv puis nous allons crée des colonnes :

- Nb_Tests_Reussis : Nb_Passed +  Nb_Na
- les Nb_Non_Testée : Nb_Not_Tested + Nb_Nmi
- Note Asqatasun : passed / (passed + failed)
- Note Asqatasun en pourcentage : Note Asqatasun * 100

### Statistiques des données

Une fois que nous avons finis les modifications nous allons réintégré les [données](EPCI_Audit_Population_.csv)
dans python pour faire une étude statistique.

```python
# Librairies utiliser
import pandas as pd
import numpy as np
from datetime import datetime

# Lire le fichier
df= pd.read_csv("EPCI_Audit_Population_.csv", delimiter=",", low_memory=False)

# Calcul de la médiane
Median = df['Nb_Failed_Occurrences'].median()

# Calcul de la moyenne
Moyenne = df['Nb_Failed_Occurrences'].mean()

# Calcul des quartiles
Quartile = df['Nb_Failed_Occurrences'].quantile([0.25,0.5,0.75])

# Calcul des déciles et ajout d'une colonne
df['Decile'] = pd.qcut(df['Nb_Failed_Occurrences'], 10, labels= False)

# Calcul de la moyenne pour chaque quartile
Quartile_1= df.loc[df['Nb_Failed_Occurrences']<=15]
Quartile_1_moy = Quartile_1['Nb_Failed_Occurrences'].mean()
Quartile_2= df.loc[(df['Nb_Failed_Occurrences']>=15)&(df['Nb_Failed_Occurrences']<=29)]
Quartile_2_moy = Quartile_2['Nb_Failed_Occurrences'].mean()
Quartile_3= df.loc[(df['Nb_Failed_Occurrences']>=29)&(df['Nb_Failed_Occurrences']<=53)]
Quartile_3_moy = Quartile_3['Nb_Failed_Occurrences'].mean()
Quartile_4= df.loc[(df['Nb_Failed_Occurrences']>=53)]
Quartile_4_moy = Quartile_4['Nb_Failed_Occurrences'].mean()

# Calcul de la moyenne de la population dans chaque quartile
Quartile_1['ptot_epci_2022'].sum()
Quartile_2['ptot_epci_2022'].sum()
Quartile_3['ptot_epci_2022'].sum()
Quartile_4['ptot_epci_2022'].sum()

# Cacul du nombre d'EPCI dans chaque quartile
Quartile_1['Lib_EPCI'].count()
Quartile_2['Lib_EPCI'].count()
Quartile_3['Lib_EPCI'].count()
Quartile_4['Lib_EPCI'].count()

# Crée une table avec les valeurs
data = {"Quartile" : ['Q1', 'Q2', 'Q3', 'Q4'],
        "Moyen_Erreur" : ['8.6', '21.5', '39.2', '99.9'],
        "Nombre_Ville" : ['278', '293', '268', '266'],
        "Population_Total" : ['23771255', '12324990', '10088616', '11752934'],
        "Date" : ['2023-01-25', '2023-01-25', '2023-01-25', '2023-01-25']}
df1 = pd.DataFrame(data=data)

# Convertir en fichier csv
df1.to_csv('Quartile_EPCI.csv')
df.to_csv('EPCI_Audit_population_decile.csv')
```

### Tranche des EPCI agrégées par département

Pour la réalisation des cartes nous allons travailler sur une tranche d'EPCI supérieur à 10000 habitants.

```python
# Librairies utilisées
import pandas as pd
import numpy as np
from datetime import datetime

# Lire le fichier
df= pd.read_csv("EPCI_Audit_population_decile.csv", delimiter=",", low_memory=False)

# Choisir que les tranches d'EPCI de plus de 10000 habitants
df1= df.loc[df['ptot_epci_2022'] > 9999]

# Pour que le calcul de la médianne soit exact mettre les erreurs par ordre
df2=df1.sort_values(by=['Nb_Failed_Occurrences'], ascending=True)

# Agrégées les EPCI par département et calculer la médiane des erreurs
df2=df1.groupby(['Région', 'Code_Dpt', 'Iso Dpt', 'Pays', 'CODE_Pays'])[['Nb_Failed_Occurrences']].median().reset_index()

# Mettre les non-conformité par ordre pour le calcul de la médiane
df3=df1.sort_values(by=['Nb_Failed'], ascending=True)

# Agrégées les EPCI par département et calculer la médiane des non_conformités
df3=df3.groupby(['Région', 'Code_Dpt', 'Iso Dpt', 'Pays', 'CODE_Pays'])[['Nb_Failed']].median().reset_index()

# Fusionner les 2 tables
df4=pd.merge(df2, df3)

# Crée une colonne et faire les calculs pour les erreurs
Erreur_carte = []
for value in df4['Nb_Failed_Occurrences'] :
    if value < 10 :
       Erreur_carte.append(0)
    elif value >=10 and value < 30 :
        Erreur_carte.append(10)
    elif value >= 30 and value <= 60 :
        Erreur_carte.append(30)
    else :
        Erreur_carte.append(60)
df4["Erreur_carte"] = Erreur_carte

# Crée une colonne et faire les calculs pour les non-conformités
Non_conformité_carte = []
for value in df4['Nb_Failed'] :
    if value <= 3 :
       Non_conformité_carte.append(0)
    elif value >3 and value < 6 :
        Non_conformité_carte.append(3)
    else :
       Non_conformité_carte.append(6)
df4["Non_conformité_carte"] = Non_conformité_carte

# Convertir en csv
df4.to_csv('Tranche_EPCI_agregees_departement_median.csv')
```

## Résultat de la base des données des EPCI avec les audits

- [Audit des EPCI par département et population](EPCI_POP_AUDIT_PASSED_NA_NON_TESTER.CSV)
- [Donnée des quartiles](Quartile_EPCI.csv)
- [Tranche des EPCI agrégées par département et calcul de la médianne](Tranche_EPCI_agregees_departement_median.csv)
