# Construction de la base de donnée de la liste des musées

Pour créer la base de données de la liste des musées, nous allons utiliser 3 fichiers :

- **Fichier 1** : [Âge et nombre de la population](Age_population.csv)
  Étape de construction voir le dossier
  [création de la base de donnée de l'âge de la population des collectivités](https://gitlab.adullact.net/adullact/observatoire-accessibilite/a11y-observatory-data/-/tree/main/source-data/Jeu_donnees_Commune_Population_age#cr%C3%A9ation-de-la-base-de-donn%C3%A9e-de-l%C3%A2ge-de-la-population-des-collectivit%C3%A9s)
- **Fichier 2** : [Listes des départements](https://www.insee.fr/fr/information/5057840)
  Fichier : Liste des départements en format csv
- **Fichier 3** :
  [Liste des Musées en France](https://data.culture.gouv.fr/explore/dataset/liste-et-localisation-des-musees-de-france/table/)
  Fichier : Télécharger en format csv

```python
# Library utiliser
import pandas as pd
import numpy as np

# Lire fichier 1
df = pd.read_csv("Age_population.csv", delimiter=",", low_memory=False)

# Lire le fichier 2
df1 = pd.read_csv("departement2021.csv", delimiter=",", low_memory=False)

# Fusionner les 2 fichiers
df2 = pd.merge(df, df1)

# Lire le fichier 3
df3 = pd.read_csv("liste-et-localisation-des-musees-de-france.csv", delimiter=";", low_memory=False)

# Supprimer les lignes vides
df3 = df3[df3['URL'].notna()]

# Fusionner les fichiers
df4 = pd.merge(df4, df2)

# Supprimer les doublons
df4.drop_duplicates()

# Réorganiser les colonnes
df4 = df4[['Identifiant Muséofile', 'Nom officiel du musée', 'Region', 'Code_Region', 'Departement',
           'Code_Dpt', 'Commune', 'Code_Commune', 'Code_Postal', 'URL', 'Latitude', 'Longitude', 'REF_Deps',
           'Population municipale', 'Population comptée à part', 'Population totale', '20_39_ans', '40_59_ans',
           'plus_60_ans']]

# Convertir en csv
df4.to_csv('Liste_musée_population_commune.csv')
```

## Résultat de la base de donnée de la liste des musées

[Base de donnée de la liste des musées](Liste_musée_population_commune.csv)
