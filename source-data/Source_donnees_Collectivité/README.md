# Source de données des collectivités

Sont regroupées ici toutes les données liées aux collectivités.

## DATA.GOUV Jeu de données BANATIC

- <https://www.data.gouv.fr/fr/datasets/base-nationale-sur-les-intercommunalites/>
- Mainteneur : DGCL Direction générale des collectivités locales

## DATA.GOUV Jeu de données Service-public.fr - Annuaire de l’administration - Base de données locales

- <https://www.data.gouv.fr/fr/datasets/service-public-fr-annuaire-de-l-administration-base-de-donnees-locales/>
- Mainteneur : Premier Ministre / DILA
- Interrogeable par API
  sur [API.GOUV.FR Annuaire des établissements publics de l'administration](https://api.gouv.fr/les-api/api_etablissements_publics)

L'API est assez pratique. Exemples :

- API Fiche de présentation : <https://api.gouv.fr/les-api/api_etablissements_publics>
- API documentation technique : <https://api.gouv.fr/documentation/api_etablissements_publics>
  (dont les URLs pour les mairies, code INSEE, coordonnées GPS) :
  - Préfectures - <http://etablissements-publics.api.gouv.fr/v3/organismes/prefecture>
  - Mairies - <http://etablissements-publics.api.gouv.fr/v3/organismes/mairie>
    - Mairies 34 - <http://etablissements-publics.api.gouv.fr/v3/departements/34/mairie>
    - Mairies 95 - <http://etablissements-publics.api.gouv.fr/v3/departements/95/mairie>
  - EPCI - <http://etablissements-publics.api.gouv.fr/v3/organismes/epci>
  - CDG - <http://etablissements-publics.api.gouv.fr/v3/organismes/cdg>
  - Conseil Régional - <http://etablissements-publics.api.gouv.fr/v3/organismes/cr>
  - Chambre d'agriculture - <http://etablissements-publics.api.gouv.fr/v3/organismes/chambre_agriculture>
  - Chambre de métiers et de l'artisanat - <http://etablissements-publics.api.gouv.fr/v3/organismes/chambre_metier>
  - CROUS - <http://etablissements-publics.api.gouv.fr/v3/organismes/crous>
  - CCI - <http://etablissements-publics.api.gouv.fr/v3/organismes/cci>
  - Inspections Académiques <https://etablissements-publics.api.gouv.fr/v3/organismes/inspection_academique>

## Commune, région et département

### 1. Communes de france - Base des codes postaux (data.gouv)

- Source : <https://www.data.gouv.fr/fr/datasets/communes-de-france-base-des-codes-postaux/>
- Fichier : `communes-departement-region.csv`
  - Liste des communes, départements et région avec la géolocalisation des communes

![](Screenshot_Communes.png)

### 2. FR Public sector - Source "INSEE : Code Officiel Géographique (COG)" + "INSEE : Découpage communal"

#### INSEE : Code Officiel Géographique (COG)

<https://www.insee.fr/fr/information/2560452> (version 2022)

- [Code officiel géographique au 1er janvier 2022](https://www.insee.fr/fr/information/6051727)
- [Liste de modalités des fichiers téléchargeables du COG](https://www.insee.fr/fr/information/6051361)
  - Type de commune (dans les fichiers « communes » et « événements sur les communes »)
  - Type d'événement sur les communes (dans le fichier « événements sur les communes »)
  - Type de nom en clair (dans tous les fichiers sauf celui concernant les pays)

Fichier disponible aussi ici :

- <https://www.data.gouv.fr/fr/datasets/code-officiel-geographique-cog/>
  - Liste des régions au 01/01/2022
  - Liste des départements au 01/01/2022
  - Liste des collectivités territoriales ayant les compétences départementales au 01/01/2022
  - Liste des communes, arrondissements municipaux, communes déléguées et communes associées au 01/01/2022
  - Liste des événements survenus aux communes, arrondissements municipaux, communes associées et commune déléguées

#### INSEE : Découpage communal

<https://www.insee.fr/fr/information/2028028>

> La table d’appartenance géographique est un fichier fourni pour toutes
> les communes et le code géographique des niveaux géographiques supérieurs auxquels
> elles appartiennent (région, département, arrondissement, canton ville, zone d'emploi,
> bassin de vie, unité urbaine, aire d'attraction des villes).
>
> La table de passage liste l’ensemble des communes ayant existé depuis le 1er janvier 2003
> et leur correspondance dans la dernière géographie disponible.

### To be dispatched ---> "code Insee des communes comme référentiel pivot" / "contours administratifs"

- <https://github.com/benoitdavidfr/comhisto> ---> Utilisation du code Insee des communes comme référentiel pivot
- <https://github.com/etalab/contours-administratifs> --> générer les principaux contours administratifs
- <https://www.data.gouv.fr/fr/datasets/base-sirene-des-etablissements-siret-geolocalisee-avec-la-base-dadresse-nationale-ban/>
  - Base SIRENE des établissements (SIRET) - géolocalisée avec la Base d'Adresse Nationale (BAN)

## FR Public sector - Source "Auracom - Listes des sites gouv.fr "

data from _Auracom_

- <https://www.data.gouv.fr/fr/datasets/listes-des-sites-gouv-fr/>
- URL <http://www.auracom.fr/gouv/sitesgouv.txt>
- URL stable    <https://www.data.gouv.fr/fr/datasets/r/24848dc0-e16c-4ce8-94d9-24bc6304c9b6>

extractor example :
[etalab/noms-de-domaine-organismes-publics > `import-auracom-opendata.py`](https://github.com/etalab/noms-de-domaine-organismes-publics/blob/master/scripts/import-auracom-opendata.py)

```python
#!/usr/bin/env python

"""Imports data from Auracom:
https://www.data.gouv.fr/fr/datasets/listes-des-sites-gouv-fr/
"""

from pathlib import Path

import requests

from sort import sort_files

ROOT = Path(__file__).resolve().parent.parent
SOURCES = ROOT / "sources"


def main():
  lines = requests.get(
    "https://www.data.gouv.fr/fr/datasets/r/24848dc0-e16c-4ce8-94d9-24bc6304c9b6"
  ).text
  with (
    open(SOURCES / "gouvfr-divers.txt", "a", encoding="UTF-8") as gouv_fr,
    open(SOURCES / "nongouvfr-divers.txt", "a", encoding="UTF-8") as nongouv_fr,
  ):
    for line in lines.splitlines():
      domain = line.split(maxsplit=1)[0]
      if domain.endswith(".gouv.fr"):
        gouv_fr.write(domain + "\n")
      else:
        nongouv_fr.write(domain + "\n")
  sort_files([SOURCES / "gouvfr-divers.txt", SOURCES / "nongouvfr-divers.txt"])


if __name__ == "__main__":
  main()
```

## FR public sector - Source "Etalab - Données decoupage administratif COG avec codes postaux et population"

Etalab - Données decoupage administratif COG avec codes postaux et population
<https://github.com/etalab/decoupage-administratif>

> Dépôt permettant de construire un paquet avec les données COG sous forme de JSON combinées avec les codes postaux de
> La Poste, les informations EPCI de la DGCL aux échelles communales, départementales, régionales ainsi
> qu’intercommunales. Ne contient pas les données géographiques. Pour ce besoin, se reporter
> à <https://github.com/etalab/contours-administratifs>
