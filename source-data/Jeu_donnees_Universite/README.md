# Construction de la base de données Université

Pour créer la base de donnée des universités, nous allons utiliser 2 fichiers :

- **Fichier 1** :
  [Enseignement supérieur](https://www.data.gouv.fr/fr/datasets/principaux-etablissements-denseignement-superieur/)
  - fr-esr-principaux-etablissements-enseignement-superieur.csv
  - Liste des écoles, Université avec leur statut, la localisation, url..
- **Fichier 2** : [Déclaration de conformité](déclaration-de-conformité-adullact.ods)
  - déclaration-de-conformité-adullact.ods
  - Listes des Universités, urls, nombre d'inscrits et déclaration d'accessibilité

Une fois que nous avons choisi les fichiers, nous allons construire la base de donnée de l'université avec python.

```python
# Library utilisées
import pandas as pd
import numpy as np
from datetime import datetime

# Lire le fichier 1
df = pd.read_csv("fr-esr-principaux-etablissements-enseignement-superieur.csv", delimiter=";", low_memory=False)

# Sélectionner que les universités
df1 = df[(df['Type_d_etablissement'] == 'Université') & (df['Pays'] == 'France')]

# Convertir le fichier en csv
df1.to_csv('Université.csv')

# Dans libre office mettre les urls sur le même format :
# =SI(STXT(B1;1;11) = "http://www.";B1;SI(STXT(B1;1;12) = "https://www.";
# CONCAT("http://www.";STXT(B1;13;NBCAR(B1)));SI(STXT(B1;9;4) <> "www.";
# CONCAT("http://www.";STXT(B1;CHERCHE("//";B1)+2;NBCAR(B1))))))`

# Lire le fichier Université
df2 = pd.read_csv("Université.csv", delimiter=",", low_memory=False)

# Supprimer des colonnes
df2 = df2.drop(columns=['URLs'])

# Lire fichier 2
df3 = pd.read_csv("déclaration-de-conformité-adullact_.csv", delimiter=",", low_memory=False)

# Supprimer des colonnes
df3 = df3.drop(columns=['URLS', 'Université'])

# Fusionner les données
df4 = pd.merge(df2, df3)
```

![](Screenshot_base_donné_université.png)

## Résultat de la base de donnée université

[Base de donnée université](Université_éleve.csv)
